package br.usp.ime.escience.expressmatch.service.locator;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public final class ServiceLocator {
	
	private static ClassPathXmlApplicationContext ctx;
	
	static {
	   ctx = new ClassPathXmlApplicationContext("../applicationContext.xml");
	}
	
	public static Object getService(String serviceName){
		return ctx.getBean(serviceName);
	}
	
}
