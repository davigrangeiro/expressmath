package br.usp.ime.escience.expressmatch.service.match.evaluate;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.ws.Response;

import java.util.PriorityQueue;
import java.util.Queue;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.usp.ime.escience.expressmatch.exception.ExpressMatchException;
import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.Point;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.model.graph.Graph;
import br.usp.ime.escience.expressmatch.model.graph.Vertex;
import br.usp.ime.escience.expressmatch.model.repository.UserParameterRepository;
import br.usp.ime.escience.expressmatch.service.combinatorial.CombinatorialGeneratorServiceProvider;
import br.usp.ime.escience.expressmatch.service.expressions.ExpressionServiceProvider;
import br.usp.ime.escience.expressmatch.service.graph.utils.GraphUtils;
import br.usp.ime.escience.expressmatch.service.match.ExpressionMatchService;
import br.usp.ime.escience.expressmatch.service.match.StrokeDistance;
import br.usp.ime.escience.expressmatch.service.match.SymbolDistance;
import br.usp.ime.escience.expressmatch.service.match.evaluate.cutting.CuttingCriteria;
import br.usp.ime.escience.expressmatch.service.match.evaluate.cutting.NeuralNetworkCuttingCriteria;
import br.usp.ime.escience.expressmatch.service.match.evaluate.memo.Cache;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierRequest;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierResponse;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierService;    
import br.usp.ime.escience.expressmatch.utils.TimeRecord;


/**
 * @author davi
 *
 */
@Service
@Transactional
public class ExpressionMatchServiceProvider implements ExpressionMatchService,  Serializable{
	
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ExpressionMatchServiceProvider.class);
	

	@Autowired
	private ExpressionServiceProvider expressionService;
	
	@Autowired
	@Qualifier("neuralNetworkSymbolClassifierServiceProvider")
	private SymbolClassifierService symbolClassifierService;
	
	@Autowired
	private CombinatorialGeneratorServiceProvider combinatorialProvider;
	
	@Autowired
	private UserParameterRepository userParameterRepository;
	
	private MatchHypothesis bestMatch;
	private Cache<SymbolMatchHypothesis> memo;
	private CuttingCriteria cuttingCriteria;
	private int cutted = 0;
	private int cuttedByBestSolutionAlreadyFound = 0;
	
	@Override
	public List<Symbol> matchExpression(Expression transcription)
			throws ExpressMatchException {

		Expression model = expressionService.loadExpressionForExpressionType(transcription.getExpressionType());
		Expression copyOfExpression = copyExpression(model);
		
		Graph inputGraph = GraphUtils.createGraphByTranscription(transcription.getSymbols());
		Graph modelGraph = GraphUtils.createGraphByModel(copyOfExpression.getSymbols());
		
		double xScale = inputGraph.getWidth() / modelGraph.getWidth();

		Expression modelAux = GraphUtils.scaleModelGraphByTranscription(modelGraph, inputGraph, copyOfExpression);
		modelGraph = GraphUtils.createGraphByModel(modelAux.getSymbols());

		Map<Integer, List<Stroke>> candidateStrokes = getCandidateStrokesForEachSymbol(modelGraph, inputGraph, transcription, copyOfExpression, xScale);

		List<Symbol> symbols = new ArrayList<Symbol>();
		symbols.addAll(model.getSymbols());

		Map<Integer, Stroke> map = new HashMap<Integer, Stroke>();
		for (Symbol symbol : transcription.getSymbols()) {
			for (Stroke s :  symbol.getStrokes()) { 
				map.put(s.getStrokeId(), s);
			}
		}

		Map<String, List<SymbolClassifierResponse>> candidateHypothesis = new HashMap<String, List<SymbolClassifierResponse>>();
		candidateHypothesis = getHypothesisForSymbols(candidateStrokes, symbols, map, candidateHypothesis);
//		applyDistanceCost(candidateHypothesis, map, model, inputGraph);
		
		List<Symbol> recognizedSymbols = findBestHypothesis(candidateHypothesis, model, map);
		
		return recognizedSymbols;
	}

	private void applyDistanceCost(
			Map<String, List<SymbolClassifierResponse>> candidateHypothesis,
			Map<Integer, Stroke> map, Expression model, Graph inputGraph) {
		final float ALPHA = 0.9F;
		final float TRANSCRIPTION_DIAGONAL_SIZE = new Double(Math.pow(Math.pow(inputGraph.getWidth(), 2) + Math.pow(inputGraph.getHeight(), 2), 0.5)).floatValue();
		
		for (Symbol s : model.getSymbols()) {
			
			List<SymbolClassifierResponse> hypothesisSet = candidateHypothesis.get(s.getHref());
			for (SymbolClassifierResponse symbolClassifierResponse : hypothesisSet) {
				float distance = getDistanceBetweenHypothesisAndSymbol(s, symbolClassifierResponse, map);
				distance /= TRANSCRIPTION_DIAGONAL_SIZE;
				symbolClassifierResponse.setAgregatedCost((ALPHA * symbolClassifierResponse.getSymbolCost()) + ((1.f - ALPHA) * distance));
			
				LOGGER.info(symbolClassifierResponse.toString());
			}
			
		}
		
	}

	private float getDistanceBetweenHypothesisAndSymbol(Symbol s,
			SymbolClassifierResponse symbolClassifierResponse,
			Map<Integer, Stroke> map) {
		float distance    = Float.MAX_VALUE,
			  distanceAux = 0.f;
		
		for (int i = 0; i <symbolClassifierResponse.getPermutation().length; i++) {
			distanceAux = map.get(symbolClassifierResponse.getPermutation()[i]).getDistanceOfSymbols().get(s.getHref()).floatValue();
			if (distanceAux < distance) {
				distance = distanceAux;
			}
		}
		
		return distance;
		
	}
	


	private boolean worthChange(SymbolClassifierResponse response, Map<Integer, Symbol> strokeForSymbol) {
		float responseCost = ((response.getSymbolCost() * response.getPermutation().length)),
			  changeCost = 0.f;
		
		Symbol sAux = null;
		for (int i : response.getPermutation()) {
			
			if (strokeForSymbol.containsKey(i)) {
				sAux = strokeForSymbol.get(i);
				changeCost += sAux.getSolution().getSymbolCost() * sAux.getSolution().getPermutation().length;
			} else {
				changeCost += response.getSymbolCost();
			}
		}
		
		return responseCost <= changeCost && sAux != null;
			
	}

	private List<Symbol> findBestHypothesis(Map<String, List<SymbolClassifierResponse>> candidateHypothesis, Expression model, Map<Integer, Stroke> map) {
		List<Symbol> notRecognizedSymbol = new ArrayList<Symbol>();
		List<Stroke> notRecognizedStrokes = null;
		
		Queue<Symbol> symbolQueue = new LinkedList<>();
		for (Symbol s : model.getSymbols()) {
			symbolQueue.add(s);
		}

		Map<Integer, Symbol> strokeForSymbol = new HashMap<>();
		
		while(!symbolQueue.isEmpty()) {
			Symbol s = symbolQueue.remove();
			
			SymbolClassifierResponse best = null;
			boolean foundSolution = false;
			
			
			while (!foundSolution) {
				
				for (SymbolClassifierResponse response : candidateHypothesis.get(s.getHref())) {
					if (response.getSymbolCost() < 1.0 && (best == null || response.getSymbolCost() < best.getSymbolCost())) {
						best = response;
					}
				}
				if (null == best) {
					foundSolution = true;
					notRecognizedSymbol.add(s);
				} else {	
					
					if (worthChange(best, strokeForSymbol)) {
						for (int i : best.getPermutation()) {
							Symbol aux = strokeForSymbol.get(i);
							
							if (null != aux && null != aux.getSolution()) {
								for (int j : aux.getSolution().getPermutation()) {
									symbolQueue.add(strokeForSymbol.remove(j));
								}
								aux.setSolution(null);
							}
						}
						s.setSolution(best);
						foundSolution = true;
						
						for (int strokeI : best.getPermutation()) {
							strokeForSymbol.put(strokeI, s);
						}
					} else {
						for (Iterator<SymbolClassifierResponse> it = candidateHypothesis.get(s.getHref()).iterator() ; it.hasNext() ; ) {
							SymbolClassifierResponse response = it.next();
							if (response == best) {
								it.remove();
								break;
							}
						}
						foundSolution = true;
						notRecognizedSymbol.add(s);
					}
				}
			}
		}
		
		
		notRecognizedStrokes = getNotRecognizedStrokes(model, map);
		
//		handleOnePointStrokes(model, map, notRecognizedSymbol, notRecognizedStrokes);
		
		if (notRecognizedSymbol.size() > 0 && notRecognizedStrokes.size() > 0) {
			for (Symbol symbol : notRecognizedSymbol) {
				
				List<StrokeDistance> distances = new ArrayList<StrokeDistance>();
				for (Stroke stroke : notRecognizedStrokes) {
					distances.add(new StrokeDistance(stroke.getStrokeId(), stroke.getDistanceOfSymbols().get(symbol.getHref())));
				}
				Collections.sort(distances);
				
				int size = 1;
				int[] values = null;
				SymbolClassifierResponse bestCost = null;
				while (symbol.getSolution() == null && size <= 5 && size <= notRecognizedStrokes.size()) {

					values = new int[size];
					
					for (int j = 0; j < values.length; j++) {
						values[j] = distances.get(j).getId();
					}
						
					List<SymbolClassifierResponse> res = this.combinatorialProvider.getPermutationsResult(values, 5, this.symbolClassifierService, map, symbol);
					
					for (SymbolClassifierResponse symbolClassifierResponse : res) {
						if (null == bestCost || (symbolClassifierResponse.getSymbolCost() < bestCost.getSymbolCost() && symbolClassifierResponse.getSymbolCost() < 1.f)) {
							bestCost = symbolClassifierResponse;
						}
					}
					size++;
				}
				
				if(null != bestCost && bestCost.getSymbolCost() < 1.f) {
					symbol.setSolution(bestCost);
					for (Integer index : symbol.getSolution().getPermutation()) {
						for (Iterator<Stroke> iterator = notRecognizedStrokes.iterator(); iterator.hasNext();) {
							Stroke stroke = iterator.next();
							if (stroke.getStrokeId().equals(index)) {
								iterator.remove();
							}
						}
					}
				}
			}
		}
			
		if (notRecognizedStrokes.size() > 0) {
			for (Stroke stroke : notRecognizedStrokes) {
				
				List<SymbolDistance> distances = new ArrayList<SymbolDistance>();
				for (Entry<String, Double> entry : stroke.getDistanceOfSymbols().entrySet()) {
					distances.add(new SymbolDistance(entry.getKey(), entry.getValue()));
				}
				Collections.sort(distances);
				
				for (SymbolDistance symbolDistance : distances) {
					boolean found = false;
					float bestCost = 5.f;
					Symbol bestSymbol = null;
					SymbolClassifierResponse bestResponse = null;
					
					for (Symbol symbol : model.getSymbols()) {
						
						if (symbolDistance.getId().equalsIgnoreCase(symbol.getHref())) {
							
							System.out.println(symbol.getHref());
							int oldPermutationLength = symbol.getSolution() != null ? symbol.getSolution().getPermutation().length : 0;
							int[] values = new int[oldPermutationLength + 1];
							for (int i = 0; i < oldPermutationLength; i++) {
								values [i] = symbol.getSolution().getPermutation()[i];
							}
							values [values.length -1] = stroke.getStrokeId();
							
							//getting the list of strokes found by the permutation.
							List<Stroke> strokesForPermutation = new ArrayList<Stroke>();
							for (int strokeIndex : values) {
								strokesForPermutation.add(map.get(strokeIndex));
								
							}
							
							//setting the attributes and send it to classifier.
							SymbolClassifierRequest<List<Stroke>> request = new SymbolClassifierRequest<>();
							request.setsTranscription(strokesForPermutation);
							request.setsModel(symbol);
							
							SymbolClassifierResponse res = symbolClassifierService.matchTranscription(request);
							//keep the permutation that was used to constitute the transcription hypothesis
							res.setPermutation(ArrayUtils.clone(values));
							
							if (res.getSymbolCost() < bestCost && 
							   (symbol.getSolution() == null || 
								res.getSymbolCost() < symbol.getSolution().getSymbolCost()) &&
								res.getSymbolCost() < 1.f && 
								res.isIdInPermutation(stroke.getStrokeId())) {
								
								bestCost = res.getSymbolCost();
								bestResponse = res;
								bestSymbol = symbol;
								found = true;
							}	
						}
					}
					
					if (found) {
						bestSymbol.setSolution(bestResponse);
					}
					
				}
					
			}
			
			for (Symbol s : model.getSymbols()) {
				if (null == s.getSolution()) {
					LOGGER.warn(MessageFormat.format("No solution found for symbol {0}", s.getHref()));
				} else {
					LOGGER.warn(MessageFormat.format("Best solution found for symbol {0}: {1}", s.getHref(), s.getSolution().toString()));
				}
			}
			
		}
		
		
		
		List<Symbol> res = generateResultsFromSymbols(model, map);
		
		return res;
	}

	private void handleOnePointStrokes(Expression model, Map<Integer, Stroke> map, List<Symbol> notRecognizedSymbol, List<Stroke> notRecognizedStrokes) {
		if (notRecognizedStrokes.size() > 0) {
			boolean remove = false;
			Iterator<Stroke> iterator = notRecognizedStrokes.iterator();
			SINGLE_POINT_STROKE: for (; iterator.hasNext();) {
				if (remove) {
					iterator.remove();
				}
				Stroke stroke = iterator.next();
				remove = false;
				if (1 == stroke.getPoints().size()) {
					Point alonePoint = stroke.getPoints().get(0);
					
					for (Symbol s : model.getSymbols()) {
						if (null != s.getSolution()) {
							for (Integer strokeId : s.getSolution().getPermutation()) {
								Stroke current = map.get(strokeId);
								for (Point p : current.getPoints()) {
									if (p.isSamePoint(alonePoint)) {
										
										int oldPermutationLength = s.getSolution() != null ? s.getSolution().getPermutation().length : 0;
										int[] newPermutation = new int[oldPermutationLength + 1];
										for (int i = 0; i < oldPermutationLength; i++) {
											newPermutation [i] = s.getSolution().getPermutation()[i];
										}
										newPermutation [newPermutation.length -1] = stroke.getStrokeId();
										
										s.getSolution().setPermutation(newPermutation);
										remove = true;
										continue SINGLE_POINT_STROKE;
									}
								}
								remove = stroke.isPointInsideBoundingBox(alonePoint);
								if (remove) {
									continue SINGLE_POINT_STROKE;
								}
							}
						}
					}
				}
			}
			if (remove) {
				iterator.remove();
			}
		}
	}

	private List<Symbol> generateResultsFromSymbols(Expression model,
			Map<Integer, Stroke> map) {
		List<Symbol> res = new ArrayList<>();  
		for (Symbol s : model.getSymbols()) {
			if (null != s.getSolution()) {
				res.add(generateResultFromSymbolClassifierResponse(s.getSolution(), map));
			}
		}
		return res;
	}

	private List<Stroke>  getNotRecognizedStrokes(Expression model, Map<Integer, Stroke> map) {

		List<Stroke> notRecognizedStrokes = new ArrayList<Stroke>();
		
		for (Entry<Integer, Stroke> strokeEntry : map.entrySet()) {
			Integer key = strokeEntry.getKey();
			boolean found = false;
			
			SYMBOL_LOOP: for (Symbol s : model.getSymbols()) {
				
				if (s.getSolution() != null) 
					for (Integer permutationValue : s.getSolution().getPermutation()) {
						found = permutationValue.equals(key);
						
						if (found) {
							break SYMBOL_LOOP;
						}
					}
				}
			
			if (!found) {
				notRecognizedStrokes.add(strokeEntry.getValue());
			}
		}
		return notRecognizedStrokes;
	}
	

	private Map<String, List<SymbolClassifierResponse>> getHypothesisForSymbols(
			Map<Integer, List<Stroke>> candidateStrokes, List<Symbol> symbols,
			Map<Integer, Stroke> map,
			Map<String, List<SymbolClassifierResponse>> candidateHypothesis) {
		
		TimeRecord timeRecord = TimeRecord.begin("Evaluating symbol hipotesis");
		for (Symbol symbol : symbols) {
			LOGGER.info("Symbol: " + symbol.getId() + " -> " + symbol.getHref());
			int[] values =  new int[candidateStrokes.get(symbol.getId()).size()];
			int i = 0;
			
			for (Stroke s : candidateStrokes.get(symbol.getId())) {
				values[i++] = s.getStrokeId();
			}
			
			LOGGER.info("Set length for symbol " + symbol.getLabel() + " - (" + symbol.getHref() + "): " + values.length + ". Values:" + Arrays.toString(values));
			LOGGER.info("Results for hypothesis set");
			
			List<SymbolClassifierResponse> res = this.combinatorialProvider.getPermutationsResult(values, 5, this.symbolClassifierService, map, symbol);
			for (SymbolClassifierResponse symbolClassifierResponse : res) {
				LOGGER.info(symbolClassifierResponse.toString());
			}
			candidateHypothesis.put(symbol.getHref(), res);
		}
		timeRecord.end();
		
		return candidateHypothesis;
	}

	private List<Symbol> generateResultsFromMatchHypothesis(
			MatchHypothesis hypothesis) {
		List<Symbol> recognizedSymbols = new ArrayList<Symbol>();
		for (Entry<Symbol, SymbolMatchHypothesis> symbol : hypothesis.hypothesis.entrySet()) {
			Symbol toAdd = new Symbol();
			toAdd.setHref(symbol.getKey().getHref());
			toAdd.setLabel(symbol.getKey().getLabel());
			toAdd.setStrokes(symbol.getValue().strokes);
			recognizedSymbols.add(toAdd);
		}
		return recognizedSymbols;
	}
	
	private Symbol generateResultFromSymbolClassifierResponse(SymbolClassifierResponse response, Map<Integer, Stroke> map) {
		Symbol res = new Symbol();
		res.setHref(response.getUsedSymbol().getHref());
		res.setLabel(response.getUsedSymbol().getLabel());
		res.setStrokes(new ArrayList<Stroke>());
		
		for (int i = 0; i < response.getPermutation().length; i++) {
			res.getStrokes().add(map.get(response.getPermutation()[i]));
		}

		return res;
	}
	
	private Expression copyExpression(Expression expression) {
		Expression res = new Expression();
		res.setId(expression.getId());
		res.setLabel(expression.getLabel());
		res.setExpressionType(expression.getExpressionType());
		res.setExpressionTransientId(expression.getExpressionTransientId());
		res.setUserInfo(expression.getUserInfo());
		
		for (Symbol symbol : expression.getSymbols()) {
			res.addCheckingBoundingBox(new Symbol(symbol));
		}
		return res;
	}

	private MatchHypothesis matchTranscription(List<Symbol> modelSymbols, Map<Integer, List<Stroke>> strokeCandidates) {
		initValues();
		
		//finding the best match
		int sizeOfCandidates = 0;
		for (Entry<Integer, List<Stroke>> s : strokeCandidates.entrySet()) {
			sizeOfCandidates += s.getValue().size();
		}
		LOGGER.info(MessageFormat.format("Estimated size of match hypothesis: {0}, sum of all buckets size: {1}", Math.pow(2, sizeOfCandidates), sizeOfCandidates));
		//cleaning already found solutions
		for (Symbol symbol : modelSymbols) {
			 symbol.setSolutionFound(null);
		}
		
		matchTranscription(modelSymbols, strokeCandidates, 0, 0, null);
		
		LOGGER.info(MessageFormat.format("Best match found: \n{0}", bestMatch.toString()));
		LOGGER.info(MessageFormat.format("Cutted subtrees by solution already found: {0}", cuttedByBestSolutionAlreadyFound));
		LOGGER.info(MessageFormat.format("Cutted subtrees by threshold: {0}", cutted));
		cleanValues();

		return bestMatch;
	}

	private void cleanValues() {
		this.memo = null;
	}

	private void initValues() {
		bestMatch = null;
		cutted = 0;
		
//		cuttingCriteria = new ShapeContextCuttingCriteria();
		cuttingCriteria = new NeuralNetworkCuttingCriteria();
		this.memo = new Cache<SymbolMatchHypothesis>();
	}
	
	private void matchTranscription(List<Symbol> modelSymbols, Map<Integer, List<Stroke>> strokeCandidates, int symbolIndex, int strokeIndex, MatchHypothesis currentHypothesis) {

		for (int i = symbolIndex; i < modelSymbols.size() ; i++) {
			Symbol currentSymbol = modelSymbols.get(i);
			List<Stroke> probableStrokes = strokeCandidates.get(currentSymbol.getId());
			
			if (strokeIndex < probableStrokes.size() && solutionNotFoundForSymbol(currentSymbol)) {
				for (int j = strokeIndex; j < probableStrokes.size() && solutionNotFoundForSymbol(currentSymbol); j++) {
					Stroke currentStroke = probableStrokes.get(j);
					
					//Without Stroke
					if (solutionNotFoundForSymbol(currentSymbol)) {
						matchTranscription(modelSymbols, strokeCandidates, i, j + 1, new MatchHypothesis(currentHypothesis, symbolClassifierService));
					} else {
						cuttedByBestSolutionAlreadyFound++;
					}
					
					//With Stroke
					if (solutionNotFoundForSymbol(currentSymbol)) {
						if (null == currentHypothesis || currentHypothesis.canAddStrokeToHypothesis(currentStroke)) {
							MatchHypothesis hypothesisToEvaluate = new MatchHypothesis(currentHypothesis, symbolClassifierService);
							hypothesisToEvaluate.addStrokeToSymbolHypothesis(currentStroke, currentSymbol);
							matchTranscription(modelSymbols, strokeCandidates, i, j + 1, hypothesisToEvaluate);
						}
					} else {
						cuttedByBestSolutionAlreadyFound++;
					}
					
				}
			} else {
				if(null != currentHypothesis ) {
					
					if(solutionNotFoundForSymbol(currentSymbol)) {
					
						strokeIndex = 0;
						//complete symbol match hypothesis
						if(!currentHypothesis.isHypothesisEmpty(currentSymbol)) {
							//calculate the cost of match the symbol
							//verify if the solution has already done for the instance
							SymbolMatchHypothesis symbolMatchHypothesis = currentHypothesis.hypothesis.get(currentSymbol);
							
							if(this.memo.containsValue(symbolMatchHypothesis) ) {
								symbolMatchHypothesis.cost = this.memo.getValueFor(symbolMatchHypothesis).cost;
		
								if (cuttingCriteria.isToCutHypothesis(symbolMatchHypothesis)) {
									cutted++;
									return;
								}
								
							} else {
								symbolMatchHypothesis.evaluateCost();
								this.memo.add(symbolMatchHypothesis);
								
								if (cuttingCriteria.isToCutHypothesis(symbolMatchHypothesis)) {
									cutted++;
									return;
								} else if (0.2 <= symbolMatchHypothesis.cost){
									currentSymbol.setSolutionFound(symbolMatchHypothesis);
								}
							}
							
						} else {
							//empty hypothesis, there is no need of continue the match of this subtree
							return;
						}
						
					} else {
						currentHypothesis.addSymbolHypothesis(currentSymbol.getSolutionFound());

						strokeIndex = 0;
						cuttedByBestSolutionAlreadyFound++;
					}
				}
			}
		}
			
		if (null != currentHypothesis) {	
			//complete matching, so calculate cost;
			currentHypothesis.calculateCost();
			if ( null == bestMatch || 
				 isHypothesisListBigger(currentHypothesis, bestMatch) || 
				 isMatchingCostBetter(currentHypothesis, bestMatch) || 
				 isCurrentHypothesisUsedStrokesListBigger(currentHypothesis, bestMatch) ) {
				
				//point best to current hypothesis
				bestMatch = currentHypothesis; 
				LOGGER.info(MessageFormat.format("Better solution found: \n{0}" ,currentHypothesis.toString()));
			}
		}
	}

	private boolean solutionNotFoundForSymbol(Symbol currentSymbol) {
		return currentSymbol.getSolutionFound() == null;
	}

	private boolean isCurrentHypothesisUsedStrokesListBigger(MatchHypothesis currentHypothesis, MatchHypothesis currentBetter) {
		return currentHypothesis.hypothesis.size() >= currentBetter.hypothesis.size() && currentHypothesis.getStrokeAmount() > currentBetter.getStrokeAmount() && currentHypothesis.cost < 1.0;
	}

	private boolean isMatchingCostBetter(MatchHypothesis currentHypothesis, MatchHypothesis currentBetter) {
		return currentHypothesis.cost < currentBetter.cost  && currentHypothesis.hypothesis.size() >= currentBetter.hypothesis.size() && currentHypothesis.getStrokeAmount() >= currentBetter.getStrokeAmount();
	}

	private boolean isHypothesisListBigger(MatchHypothesis currentHypothesis, MatchHypothesis currentBetter) {
		return currentHypothesis.hypothesis.size() > currentBetter.hypothesis.size();
	}
	 		
	private Map<Integer, List<Stroke>> getCandidateStrokesForEachSymbol(Graph modelGraph, Graph transcriptionGraph, 
																		Expression transcription, Expression modelExpression, Double xScale) {
		Map<Integer, List<Stroke>> res = new  HashMap<Integer, List<Stroke>>();
		
		Point2D modelMinPositions = modelGraph.getMinPositions(); 
		Point2D transcriptionMinPositions = transcriptionGraph.getMinPositions();
		
		double  modelWidth  = modelGraph.getWidth(),
				modelHeight = modelGraph.getHeight(),
				transcriptionWidth  = transcriptionGraph.getWidth(),
				transcriptionHeight = transcriptionGraph.getHeight();
		
		
		for (Vertex v : modelGraph.getVertices()) {
			double symbolHorizontalDistance = Math.abs(v.getX() - modelMinPositions.getX()),
				   symbolVerticalDistance   = Math.abs(v.getY() - modelMinPositions.getY());
			
			Point newCentroidForVertex = new Point(new Double(transcriptionMinPositions.getX() + (transcriptionWidth * (symbolHorizontalDistance/modelWidth))).floatValue(),
												   new Double(transcriptionMinPositions.getY() + (transcriptionHeight * (symbolVerticalDistance/modelHeight))).floatValue());
			
			Symbol current = null;
			for (Symbol s : modelExpression.getSymbols()) {
				if(v.getId() == s.getId()) {
					current = s;
					break;
				}
			}
			res.put(v.getId(), getStrokesBasedOnSymbolModelPosition(transcription, newCentroidForVertex, current.getSymbolDiagonalSize() * (xScale.floatValue()), current.getHref()));
			
		}
		
		return res;
	}
	
	
	private List<Stroke> getStrokesBasedOnSymbolModelPosition(Expression transcription, Point centroidPosition, float symbolBoundingBoxDiagonal, String symbolHref) {
		List<Stroke> res = new ArrayList<Stroke>();
		
		Double distance = 0.0;
		float multiplier = 1.f;
		while (res.isEmpty()) {
			for (Symbol symbol : transcription.getSymbols()) {
				STROKE_LOOP: for (Stroke stroke : symbol.getStrokes()) {
					
						distance = Math.sqrt(
								Math.pow(stroke.getRepresentantPointOfStroke().getX() - centroidPosition.getX(), 2) +
								Math.pow(stroke.getRepresentantPointOfStroke().getY() - centroidPosition.getY(), 2)
							);
					
						stroke.getDistanceOfSymbols().put(symbolHref, distance);
						
						if(distance.floatValue() <= symbolBoundingBoxDiagonal * multiplier) {
							stroke.setDistance(distance);
							res.add(stroke);
							continue STROKE_LOOP;
						}
						
	//					for (Point p : stroke.getPoints()) {
	//						
	//						distance = Math.sqrt(
	//								Math.pow(p.getX() - centroidPosition.getX(), 2) +
	//								Math.pow(p.getY() - centroidPosition.getY(), 2)
	//								);
	//						if(distance.floatValue() <= symbolBoundingBoxDiagonal) {
	//							stroke.setDistance(distance);
	//							res.add(stroke);
	//							continue STROKE_LOOP;
	//						}
	//					}
				}
			}
			multiplier += 0.1f;
		}
		
		if (res.size() > 6) {
			Collections.sort(res, new Comparator<Stroke>() {

				@Override
				public int compare(Stroke o1, Stroke o2) {
					return o1.getDistance().compareTo(o2.getDistance());
				}
			});
			
			for (int i = 6; i < res.size();) {
				res.remove(i);
			}
			
		}
		
		return res;
	}
	
}
