package br.usp.ime.escience.expressmatch.service.parameter;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.usp.ime.escience.expressmatch.model.Parameter;
import br.usp.ime.escience.expressmatch.model.User;
import br.usp.ime.escience.expressmatch.model.UserInfo;
import br.usp.ime.escience.expressmatch.model.UserParameter;
import br.usp.ime.escience.expressmatch.model.repository.ParameterRepository;
import br.usp.ime.escience.expressmatch.model.repository.UserParameterRepository;
import br.usp.ime.escience.expressmatch.service.user.UserServiceProvider;

@Service
@Transactional
public class ParameterServiceProvider implements Serializable{

	private static final String ROOT_USER = "root";

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private UserParameterRepository userParameterRepository;

	@Autowired
	private UserServiceProvider userService;
	
	@Autowired
	private ParameterRepository	parameterRepository;
	
	
	public UserParameter findParameterForUser(Parameter parameter, UserInfo user) {
		return userParameterRepository.findByParameterAndUserInfo(parameter, user);
	}
	
	public List<UserParameter> findUserParameters(UserInfo user) {
		List<UserParameter> res = userParameterRepository.findByUserInfo(user);
		for (UserParameter userParameter : res) {
			userParameter.getParameter();
			userParameter.getUserInfo();
		}
		return res;
	}
	
	public List<UserParameter> findUserParameters(User user) {
		UserInfo userInfo = userService.loadUserInformation(user.getNick());
		return findUserParameters(userInfo);
	}

	@Transactional
	public List<UserParameter> findRootParameters() {
		UserInfo rootUser = userService.loadUserInformation(ROOT_USER);
		List<UserParameter> res = userParameterRepository.findByUserInfo(rootUser);
		for (UserParameter userParameter : res) {
			userParameter.getParameter();
			userParameter.getUserInfo();
		}
		return res;
	}

	public List<Parameter> findParameters() {
		return this.parameterRepository.findAll();
	}

	public void deleteUserParameter(Integer id) {
		this.userParameterRepository.delete(id);
	}

	public void addUserParameter(UserParameter newUserParameter) {
		newUserParameter.setParameter(this.parameterRepository.findOne(newUserParameter.getParameter().getId()));
		newUserParameter.setUserInfo(userService.loadUserInformation(newUserParameter.getUserInfo().getUser().getNick()));
		this.userParameterRepository.save(newUserParameter);
	}

	public void saveUserParameters(List<UserParameter> userParameters) {
		this.userParameterRepository.save(userParameters);
	}

	public void addRootParameter(UserParameter newRootParameter) {
		Parameter newParameter = newRootParameter.getParameter();
		parameterRepository.save(newParameter);
		newRootParameter.setParameter(newParameter);
		newRootParameter.setUserInfo(userService.loadUserInformation(ROOT_USER));
		this.userParameterRepository.save(newRootParameter);
	}
	
}
