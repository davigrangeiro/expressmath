package br.usp.ime.escience.expressmatch.utils;

import java.text.MessageFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TimeRecord {

	private static final Logger LOGGER = LoggerFactory.getLogger(TimeRecord.class);
	
	private Date begin;
	private Date end;
	private String processName;
	
	private TimeRecord(Date begin, String processName){
		super();
		this.begin = begin;
		this.processName = processName;
	}
	
	public static TimeRecord begin(String processName){
		return new TimeRecord(new Date(), processName);
	}
	
	public void end(){
		this.end = new Date();
		LOGGER.info(MessageFormat.format("The process {0} spent {1} miliseconds", this.processName, this.end.getTime() - this.begin.getTime()));
	}
	
	public Date getBegin() {
		return begin;
	}
	public Date getEnd() {
		return end;
	}

	public String getProcessName() {
		return processName;
	}
	
	
	
}
