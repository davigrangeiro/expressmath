package br.usp.ime.escience.expressmatch.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "match_request", catalog = "expressMatch")
public class MatchRequest  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private UserInfo userInfo;
	private Date insertDate;
	private String matchMethod;
	private String requestName;
	private String desc;
	
	private List<ExpressionMatch> expressionMatch;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "request_description", nullable=true)
	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Column(name = "request_name", nullable=true)
	public String getRequestName() {
		return requestName;
	}


	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}


	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_requester_id", nullable = false)
	public UserInfo getUserInfo() {
		return userInfo;
	}


	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}


	@Column(name = "match_method", nullable=true)
	public String getMatchMethod() {
		return matchMethod;
	}


	public void setMatchMethod(String matchMethod) {
		this.matchMethod = matchMethod;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insert_date")
	public Date getInsertDate() {
		return insertDate;
	}


	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "matchRequest", cascade=CascadeType.PERSIST)
	public List<ExpressionMatch> getExpressionMatch() {
		return expressionMatch;
	}


	public void setExpressionMatch(List<ExpressionMatch> expressionMatch) {
		this.expressionMatch = expressionMatch;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((expressionMatch == null) ? 0 : expressionMatch.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((insertDate == null) ? 0 : insertDate.hashCode());
		result = prime * result
				+ ((userInfo == null) ? 0 : userInfo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchRequest other = (MatchRequest) obj;
		if (expressionMatch == null) {
			if (other.expressionMatch != null)
				return false;
		} else if (!expressionMatch.equals(other.expressionMatch))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (insertDate == null) {
			if (other.insertDate != null)
				return false;
		} else if (!insertDate.equals(other.insertDate))
			return false;
		if (userInfo == null) {
			if (other.userInfo != null)
				return false;
		} else if (!userInfo.equals(other.userInfo))
			return false;
		return true;
	}

}
