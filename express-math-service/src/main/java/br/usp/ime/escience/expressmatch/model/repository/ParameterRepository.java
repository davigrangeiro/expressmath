package br.usp.ime.escience.expressmatch.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.usp.ime.escience.expressmatch.model.Parameter;

public interface ParameterRepository extends JpaRepository<Parameter, Long>{

}
