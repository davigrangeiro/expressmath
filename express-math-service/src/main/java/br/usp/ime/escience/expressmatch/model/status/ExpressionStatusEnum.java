package br.usp.ime.escience.expressmatch.model.status;



public enum ExpressionStatusEnum {

	EXPRESSION_TRANSCRIBED(1, "Transcribed"),
	EXPRESSION_EVALUATED(2, "Evaluated"),
	EXPRESSION_VALIDATED(3, "Validated"),
	EXPRESSION_REJECTED(4, "Rejected");
	
	
	private final Integer value;
	private final String name;
	
	public static ExpressionStatusEnum getEnumValueFromValue(int i) {
	     for (ExpressionStatusEnum status : ExpressionStatusEnum.values()) {
	         if (status.getValue() == i) {
	             return status;
	         }
	     }
	     throw new IllegalArgumentException("the given number doesn't match any Status.");
	 }
	
	private ExpressionStatusEnum(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

}
