package br.usp.ime.escience.expressmatch.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.usp.ime.escience.expressmatch.model.Parameter;
import br.usp.ime.escience.expressmatch.model.UserInfo;
import br.usp.ime.escience.expressmatch.model.UserParameter;

public interface UserParameterRepository extends JpaRepository<UserParameter, Integer>{

	List<UserParameter> findByUserInfo(UserInfo userInfo);
	
	UserParameter findByParameterAndUserInfo(Parameter parameter, UserInfo userInfo);
	
	
}
