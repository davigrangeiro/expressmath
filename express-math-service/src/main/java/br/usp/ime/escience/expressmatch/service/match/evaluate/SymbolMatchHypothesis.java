package br.usp.ime.escience.expressmatch.service.match.evaluate;

import java.util.ArrayList;
import java.util.List;

import br.usp.ime.escience.expressmatch.constants.SystemConstants;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.service.match.evaluate.memo.Cacheable;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierRequest;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierResponse;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierService;

public class SymbolMatchHypothesis implements Cacheable{

	public Symbol model;
	public List<Stroke> strokes;
	public float cost;
	private SymbolClassifierService symbolClassifierService;
	
	
	public SymbolMatchHypothesis(SymbolClassifierService symbolClassifierService, Symbol model) {
		super();
		this.strokes = new ArrayList<Stroke>();
		this.model = model;
		this.setSymbolClassifierService(symbolClassifierService);
	}


	public SymbolMatchHypothesis(SymbolClassifierService symbolClassifierService, SymbolMatchHypothesis value) {
		super();
		this.setSymbolClassifierService(symbolClassifierService);
		this.model = value.model;
		this.cost = value.cost;
		this.strokes = new ArrayList<Stroke>();
		for (Stroke stroke : value.strokes) {
			this.strokes.add(stroke);
		}
	}


	public void evaluateCost() {
		if (!isThereOnlyPointsInThePermutation(strokes)) {
			//setting the attributes and send it to classifier.
			SymbolClassifierRequest<List<Stroke>> request = new SymbolClassifierRequest<>();
			request.setsTranscription(strokes);
			request.setsModel(model);
			
			SymbolClassifierResponse res = symbolClassifierService.matchTranscription(request);
			this.cost = res.getSymbolCost();
		} else {
			this.cost = SystemConstants.MATCH_DEFAULT_VALUE;
		}
	}


	private boolean isThereOnlyPointsInThePermutation(List<Stroke> strokesForPermutation) {
		boolean res = true;
		
		for (Stroke stroke : strokesForPermutation) {
			res &= ( stroke.getPoints().size() == 1 );
		}
		
		return res;
	}


	public SymbolClassifierService getSymbolClassifierService() {
		return symbolClassifierService;
	}


	public void setSymbolClassifierService(
			SymbolClassifierService symbolClassifierService) {
		this.symbolClassifierService = symbolClassifierService;
	}


	@Override
	public String getStringKey() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(this.model.getHref()).append("::");
		for (Stroke stroke : strokes) {
			builder.append(stroke.getStrokeId()).append(";");
		}

		return builder.toString();
	}
	
}
