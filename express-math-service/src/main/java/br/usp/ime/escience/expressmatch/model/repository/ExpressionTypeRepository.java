package br.usp.ime.escience.expressmatch.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.usp.ime.escience.expressmatch.model.ExpressionType;

public interface ExpressionTypeRepository extends JpaRepository<ExpressionType, Integer> {

	@Query("select (max(t.id) + 1) from ExpressionType t")
	Integer getNextId();
	
}
