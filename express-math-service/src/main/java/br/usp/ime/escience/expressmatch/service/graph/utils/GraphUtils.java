package br.usp.ime.escience.expressmatch.service.graph.utils;

import java.util.Collection;

import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.Point;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.model.graph.Graph;
import br.usp.ime.escience.expressmatch.model.graph.Node;
import br.usp.ime.escience.expressmatch.model.graph.Vertex;

public class GraphUtils {
	
	private GraphUtils(){}
	
	public static Graph createGraphByTranscription(Collection<Symbol> symbolList){
		Graph responseGraph = new Graph();
		
		int i=0;
		for (Symbol symbol : symbolList) {
			for (Stroke stroke : symbol.getStrokes()) {
			
				Point representant = stroke.getRepresentantPointOfStroke();
				responseGraph.addVertex(i++, stroke.getStrokeId(), representant.getX(), representant.getY());
				
			}
		}
		
		return responseGraph;
	}
	
	public static Graph createGraphByModel(Collection<Symbol> symbolList){
		Graph responseGraph = new Graph();
		
		int i=0;
		for (Symbol symbol : symbolList) {
			Point representant = symbol.getSymbolCentroid();
			responseGraph.addVertex(symbol.getId(), representant.getX(), representant.getY());
		}
		
		return responseGraph;
	}
	
	public static Expression scaleModelGraphByTranscription(Graph model, Graph transcription, Expression modelExpression) {
		double xScale = transcription.getWidth() / model.getWidth();
		double yScale = transcription.getHeight() / model.getHeight();
		return scaleExpressionByFactors(modelExpression, xScale, yScale);
	}
	
	public static Expression scaleExpressionByFactors(Expression in, double xScale, double yScale) {
		double xOffset = 0,
			   yOffset = 0;
		
		for (Symbol s : in.getSymbols()) {
			Point centroid = s.getSymbolCentroid();
			xOffset = (centroid.getX() * xScale) - centroid.getX(); 
			yOffset = (centroid.getY() * yScale) - centroid.getY();
			
			s.translateSymbol(xOffset, yOffset);

			s.getSymbolDiagonalSize();
		}
		
		return in;
	}
	
	
	public static Node getTreeRoot(Node[] nodeList){
		Node res = null;
		
		for (int i = 0; i < nodeList.length && null == res; i++) {
			if(null == nodeList[i].getPrevious()) {
				res = nodeList[i];
			}
		}
		
		return res;
	}
	
}