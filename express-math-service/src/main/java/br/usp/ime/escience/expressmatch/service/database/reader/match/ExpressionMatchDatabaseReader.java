package br.usp.ime.escience.expressmatch.service.database.reader.match;

import java.io.BufferedReader;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.usp.ime.escience.expressmatch.utils.FileUtils;

public class ExpressionMatchDatabaseReader {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExpressionMatchDatabaseReader.class);
	
	private static final String EXPRESSION_MATCH_PATH = "/opt/expressMath/database/expressionMatch/";
	private static final String EXPRESSION_MATCH_DATA_FILENAME = "matchingsExpressMatch";
	
	public Map<ExpressionMatchData, ExpressionMatchData> getExpressionMatchData() {
		LOGGER.info("Start reading of mathemathical expression match");
		
		Map<ExpressionMatchData, ExpressionMatchData> res = new HashMap<ExpressionMatchData, ExpressionMatchData>();
		String line = "";
		String[] users = null;
		int type = 0;
		
		try (BufferedReader expressionDataReader = FileUtils.getBufferedReaderFromFile(new File(EXPRESSION_MATCH_PATH + EXPRESSION_MATCH_DATA_FILENAME))) {
			ExpressionMatchData record = new ExpressionMatchData();
			while (expressionDataReader.ready()) { 
				
				line = expressionDataReader.readLine();
				
				if (!line.isEmpty() && line.startsWith("Expression:")) {
					type = Integer.valueOf(line.substring(11).trim());
				} else if (!line.isEmpty() && line.startsWith("user0")){
					users = line.split("\t");
					
					for (String user : users) {
						record = new ExpressionMatchData(type, user.toLowerCase());
						res.put(record, record);
					}
					
				} else if (!line.isEmpty()){
					String[] values = line.split("\t");
					
					for (int i = 0; i < values.length; i++) {
						record.setType(type);
						record.setUser(users[i].toLowerCase());
						
						res.get(record).getSymbolMatch().put(Integer.valueOf(values[0]), Integer.valueOf(values[i]));
					}
				}
			
			}
			
		} catch (Exception e) {
			LOGGER.error("There was an error while importing data aboult expression match", e);
		}
		
		LOGGER.info("Finished reading of mathemathical expression match");
		return res;
	}
	
}
