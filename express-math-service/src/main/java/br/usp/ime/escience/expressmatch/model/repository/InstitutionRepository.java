package br.usp.ime.escience.expressmatch.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.usp.ime.escience.expressmatch.model.Institution;

public interface InstitutionRepository extends JpaRepository<Institution, Integer> {

}
