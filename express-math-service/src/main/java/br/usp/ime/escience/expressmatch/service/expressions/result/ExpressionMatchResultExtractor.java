package br.usp.ime.escience.expressmatch.service.expressions.result;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.ExpressionMatch;
import br.usp.ime.escience.expressmatch.model.ExpressionType;
import br.usp.ime.escience.expressmatch.model.MatchRequest;
import br.usp.ime.escience.expressmatch.model.SimulatedExpressionMatch;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.model.SymbolMatch;
import br.usp.ime.escience.expressmatch.model.User;
import br.usp.ime.escience.expressmatch.model.UserInfo;
import br.usp.ime.escience.expressmatch.model.repository.ExpressionMatchRepository;
import br.usp.ime.escience.expressmatch.model.repository.ExpressionTypeRepository;
import br.usp.ime.escience.expressmatch.model.repository.MatchRequestRepository;
import br.usp.ime.escience.expressmatch.model.repository.SimulatedExpressionMatchRepository;
import br.usp.ime.escience.expressmatch.model.repository.UserInfoRepository;
import br.usp.ime.escience.expressmatch.model.status.ExpressionStatusEnum;
import br.usp.ime.escience.expressmatch.service.expressions.ExpressionServiceProvider;
import br.usp.ime.escience.expressmatch.service.match.ExpressionMatchService;

@Service
public class ExpressionMatchResultExtractor  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ExpressionMatchResultExtractor.class);
	
	@Autowired
	private ExpressionTypeRepository expressionTypeRepository;
	
	@Autowired
	private ExpressionServiceProvider expressionService;
	
	@Autowired
	private UserInfoRepository userInfoRepository;
	
	@Autowired 
	@Qualifier("expressionMatchServiceProvider")
	private ExpressionMatchService expressionMatchService;
	
	@Autowired
	private ExpressionMatchRepository expressionMatchRepository;

	@Autowired
	private MatchRequestRepository matchRequestRepository;
	
	@Autowired
	private SimulatedExpressionMatchRepository simulatedExpressionMatchRepository;
	
	@Async
	public void extractExpressionDatabaseResults(User user, MatchRequest matchRequest){
		try {
			
			LOGGER.info("Starting Expression DataBase result extractor");
			List<ExpressionType> types = this.expressionTypeRepository.findAll();
			UserInfo userInfo = this.userInfoRepository.getUserInfoByUserNick(user.getNick());
			LOGGER.info(MessageFormat.format("Found {0} expression types", types.size()));
			
			matchRequest.setUserInfo(userInfo);
			matchRequestRepository.save(matchRequest);
			
			int i = 1;
			while (!types.isEmpty()) {
				
				ExpressionType expressionType = types.remove(0);
				
				LOGGER.info(MessageFormat.format("Simulating math of model expression {0}, ({1} of {2})", expressionType.getLabel(), i++, types.size()));
				
				extractExpressionResultByType(expressionType, userInfo, matchRequest);

				LOGGER.info(MessageFormat.format("Saved {0} simulated matches of model expression {1}", i, expressionType.getLabel()));
			}
			
	
			LOGGER.info("Finished Expression DataBase result extractor");
			
		} catch (Exception e) {
			LOGGER.error("Error while trying to save expressions: ", e);
		}
	}

	@Transactional
	private void saveResults(SimulatedExpressionMatch simulatedExpressionMatch) {
		LOGGER.info("Saving results");
		this.expressionMatchRepository.save(simulatedExpressionMatch.getMatch());
		simulatedExpressionMatch.setId(simulatedExpressionMatch.getMatch().getId());
		this.simulatedExpressionMatchRepository.save(simulatedExpressionMatch);
	}
	
	@Transactional
	public void extractExpressionResultByType(ExpressionType type, UserInfo user, MatchRequest matchRequest) {
		List<Expression> expressions = expressionService.getValidatedExpressionsByExpressionType(type);
		LOGGER.info(MessageFormat.format("Found {0} expression of type {1}", expressions.size(), type.getLabel()));
		
		for (Expression expression : expressions) {
			saveResults(matchExpression(type, expression, user, matchRequest));
		}
	}

	private SimulatedExpressionMatch matchExpression(ExpressionType type, Expression expression, UserInfo user, MatchRequest matchRequest) {

		Expression typeExpression = expressionService.loadExpressionForExpressionType(type);
		LOGGER.info(MessageFormat.format("Starting match of model {0} with expression {1} for user {2}", type.getLabel(), expression.getId(), user.getName()));
		SimulatedExpressionMatch simulatedMatch = new SimulatedExpressionMatch();
		simulatedMatch.setModelSize(typeExpression.getSymbols().size());
		simulatedMatch.setCorrectMatchedSize(0);
		
		ExpressionMatch match = new ExpressionMatch();
		match.setExpression(expression);
		match.setExpressionType(type);
		match.setMatchRequest(matchRequest);
		match.setSymbolMatching(new ArrayList<SymbolMatch>());
		match.setUserInfo(user);
		
		simulatedMatch.setMatch(match);
		
		if(isValidExpression(type, expression))  {
			
			try {
				List<Symbol> symbols = expressionMatchService.matchExpression(expression);
				
				for (Symbol s : symbols) {
					
					SymbolMatch symbolMatch = new SymbolMatch();
					symbolMatch.setSameClass(Boolean.FALSE);
					symbolMatch.setSameReference(Boolean.FALSE);
					symbolMatch.setExpressionMatch(match);
					
					for (Symbol	model: expression.getSymbols()) {
						
						if (isSameSymbol(model, s)) {
							simulatedMatch.setCorrectMatchedSize(simulatedMatch.getCorrectMatchedSize() + 1);
							symbolMatch.setSameReference(Boolean.TRUE);
							symbolMatch.setSameClass(Boolean.TRUE);
							symbolMatch.setModelSymbol(model);
						}
					}
					
					match.getSymbolMatching().add(symbolMatch);
				}
				
				
			} catch (Exception e) {
				LOGGER.error("There as an error while matching expression type: " + type.getLabel(), e);
			}
		}
		LOGGER.info("Match finished");
		return simulatedMatch;
	}
	
	public SimulatedExpressionMatch getResultForMatch(Expression groundTruth, List<Symbol> recognizedSymbols) {

		groundTruth = this.expressionService.findById(groundTruth.getId());
		
		for (Symbol symbol : recognizedSymbols) {
			for (Stroke stroke : symbol.getStrokes()) {
				stroke.setId(stroke.getStrokeId());
			}
		}
		
		StringBuilder builder = new StringBuilder();
		for (Symbol symbol : groundTruth.getSymbols()) {
			builder.append("Symbol: " + symbol.getHref() + "\n Strokes: ");
			for (Stroke stroke : symbol.getStrokes()) {
				builder.append(stroke.getId() + "  ");
			}
			builder.append("\n");
		}
		
		LOGGER.info(builder.toString());
		
		SimulatedExpressionMatch simulatedMatch = new SimulatedExpressionMatch();
		simulatedMatch.setModelSize(groundTruth.getSymbols().size());
		simulatedMatch.setCorrectMatchedSize(0);
		
		try {
			
			for (Symbol s : recognizedSymbols) {
				for (Symbol	modelSymbol: groundTruth.getSymbols()) {
					if (isSameSymbol(modelSymbol, s)) {
						simulatedMatch.setCorrectMatchedSize(simulatedMatch.getCorrectMatchedSize() + 1);
						break;
					}
				}
			}
			
				
		} catch (Exception e) {
			LOGGER.error("There as an error while matching expression type: " + groundTruth.getLabel(), e);
		}
		
		return simulatedMatch;
	}

	private boolean isSameSymbol(Symbol model, Symbol s) {
		boolean res = false;
		if (s.getHref().equals(model.getHref()) && s.getStrokes().size() == model.getStrokes().size()) {
			res = true;
			for (int i = 0; i < s.getStrokes().size() && res; i++) {
				Stroke stroke = s.getStrokes().get(i);
				boolean strokePresent = false;
				
				for (Stroke modelStroke : model.getStrokes()) {
					if(stroke.getId().equals(modelStroke.getId())) {
						strokePresent = true;
						break;
					}
				}
				res &= strokePresent;
			}
		}
		return res;
	}

	private boolean isValidExpression(ExpressionType type, Expression expression) {
		return null != expression && null != type.getExpression() && 
		   expression.getId().longValue() != type.getExpression().getId() && 
		   expression.getExpressionStatus().intValue() == ExpressionStatusEnum.EXPRESSION_VALIDATED.getValue();
	}
	
}
