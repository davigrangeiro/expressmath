package br.usp.ime.escience.expressmatch.service.match.evaluate;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierService;

public class MatchHypothesis {

	private SymbolClassifierService symbolClassifierService;
	public Map<Symbol, SymbolMatchHypothesis> hypothesis;
	public float cost;
	
	
	public MatchHypothesis(MatchHypothesis currentHipotesis,SymbolClassifierService symbolClassifierService) {
		this.symbolClassifierService = symbolClassifierService;
		hypothesis = new HashMap<Symbol, SymbolMatchHypothesis>();
		if (null != currentHipotesis && null != currentHipotesis.hypothesis) {
			for (Entry<Symbol, SymbolMatchHypothesis> currentSymbolHipotesis : currentHipotesis.hypothesis.entrySet()) {
				SymbolMatchHypothesis toAdd = new SymbolMatchHypothesis(this.symbolClassifierService, currentSymbolHipotesis.getValue());
				this.hypothesis.put(toAdd.model, toAdd);
			}
		}
	}

	public boolean canAddStrokeToHypothesis(Stroke stroke) {
		boolean res = true;
		
		for (Entry<Symbol, SymbolMatchHypothesis> currentHipotesis : hypothesis.entrySet()) {
			for (Stroke currentStroke : currentHipotesis.getValue().strokes) {
				if (stroke.getStrokeId().equals(currentStroke.getStrokeId())) {
					res = false;
					break;
				}
			}
		}
		return res;
	}
	
	public boolean isHypothesisEmpty(Symbol s) {
		return !hypothesis.containsKey(s) || hypothesis.get(s).strokes == null ||  hypothesis.get(s).strokes.isEmpty();
	}

	public void calculateCost() {
		this.cost = 0.f;
		for (Entry<Symbol, SymbolMatchHypothesis> currentHipotesis : hypothesis.entrySet()) {
			if (0.f == currentHipotesis.getValue().cost) {
				currentHipotesis.getValue().evaluateCost();
			}
			this.cost += currentHipotesis.getValue().cost;
		}
	}
	
	public int getStrokeAmount() {
		int res = 0;
		for (Entry<Symbol, SymbolMatchHypothesis> currentHipotesis : hypothesis.entrySet()) {
			res += currentHipotesis.getValue().strokes.size();
		}
		return res;
	}

	public void addStrokeToSymbolHypothesis(Stroke currentStroke, Symbol currentSymbol) {
		
		SymbolMatchHypothesis current = hypothesis.remove(currentSymbol);
		if (null == current) {
			current = new SymbolMatchHypothesis(this.symbolClassifierService, currentSymbol);
		}
		current.strokes.add(currentStroke);
		hypothesis.put(currentSymbol, current);
	}
	
	public void addSymbolHypothesis(SymbolMatchHypothesis smHypothesis) {
		hypothesis.put(smHypothesis.model, smHypothesis);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("MatchHypothesis [ hypothesis= [ \nsymbol label:");
		
		for (Entry<Symbol, SymbolMatchHypothesis> currentHipotesis : hypothesis.entrySet()) {
			builder.append(currentHipotesis.getKey().getHref()).append(" id= ").append(currentHipotesis.getKey().getId()).append(" strokes [");
			for (Stroke s : currentHipotesis.getValue().strokes) {
				builder.append(s.getId()).append(" - ").append(s.getStrokeId()).append(";");
			}
			builder.append(" ] cost= ").append(currentHipotesis.getValue().cost).append(" \nsymbol label:");
		}
		builder.append("] total cost= ").append(this.cost);
		return builder.toString();
	}
}
