package br.usp.ime.escience.expressmatch.model;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "parameter", catalog = "expressMatch")
public class Parameter implements java.io.Serializable {


	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String parameterType;
	private String parameterName;
	private String parameterDesription;
	private Date insertDate;


	public Parameter() {
		super();
	}

	public Parameter(Long id, String pameterType, String parameterName,
			String parameterDesription, Date insertDate) {
		super();
		this.id = id;
		this.parameterType = pameterType;
		this.parameterName = parameterName;
		this.parameterDesription = parameterDesription;
		this.insertDate = insertDate;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "parameter_type", nullable = false)
	public String getParameterType() {
		return parameterType;
	}

	public void setParameterType(String parameterType) {
		this.parameterType = parameterType;
	}

	@Column(name = "parameter_name", nullable = false)
	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	@Column(name = "parameter_description", nullable = false)
	public String getParameterDesription() {
		return parameterDesription;
	}

	public void setParameterDesription(String parameterDesription) {
		this.parameterDesription = parameterDesription;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insert_date", length = 19)
	public Date getInsertDate() {
		return this.insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((insertDate == null) ? 0 : insertDate.hashCode());
		result = prime * result
				+ ((parameterType == null) ? 0 : parameterType.hashCode());
		result = prime
				* result
				+ ((parameterDesription == null) ? 0 : parameterDesription
						.hashCode());
		result = prime * result
				+ ((parameterName == null) ? 0 : parameterName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parameter other = (Parameter) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (insertDate == null) {
			if (other.insertDate != null)
				return false;
		} else if (!insertDate.equals(other.insertDate))
			return false;
		if (parameterType == null) {
			if (other.parameterType != null)
				return false;
		} else if (!parameterType.equals(other.parameterType))
			return false;
		if (parameterDesription == null) {
			if (other.parameterDesription != null)
				return false;
		} else if (!parameterDesription.equals(other.parameterDesription))
			return false;
		if (parameterName == null) {
			if (other.parameterName != null)
				return false;
		} else if (!parameterName.equals(other.parameterName))
			return false;
		return true;
	}

}
