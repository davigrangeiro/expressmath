package br.usp.ime.escience.expressmatch.service.symbol.classifier.neural.net;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.usp.ime.escience.expressmatch.model.Point;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierRequest;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierResponse;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierService;
import br.usp.ime.vision.frank.classifier.Classifiable;
import br.usp.ime.vision.frank.classifier.ClassificationResult;
import br.usp.ime.vision.frank.classifier.NeuralNetworkClassifier;
import br.usp.ime.vision.frank.classifier.NeuralNetworkFeatures;
import br.usp.ime.vision.frank.classifier.TimePoint;

@Service
@Transactional
public class NeuralNetworkSymbolClassifierServiceProvider implements SymbolClassifierService,  Serializable{
	
	private static final long serialVersionUID = 1L;

	private static final String FRAC = "\\frac";

	@Override
	public SymbolClassifierResponse matchTranscription(
			SymbolClassifierRequest<List<Stroke>> request) {
        
		br.usp.ime.vision.frank.classifier.Symbol classifierSymbol = createClassifierSymbol(request.getsTranscription());
        Double cost = extractCost(request, classifierSymbol);
        SymbolClassifierResponse res = new SymbolClassifierResponse();
        res.setUsedSymbol(request.getsModel());
        res.setSymbolCost(cost.floatValue());
        
        return res;
	}

	private Double extractCost(SymbolClassifierRequest<List<Stroke>> request,
			br.usp.ime.vision.frank.classifier.Symbol classifierSymbol) {
		//TO CLASSIFY THE SYMBOL        
        Classifiable classifible = new Classifiable();
        double[] features = NeuralNetworkFeatures.extractMergedFeatures(classifierSymbol);
        classifible.setFeatures(features);
        NeuralNetworkClassifier classifier = NeuralNetworkClassifierSingleton.getInstance();
        classifier.classify(classifible);
        ClassificationResult result = classifier.getClassificationResult();
        result.ascendingSortByConfidence();
        
        Double cost = 5.0;
        String modelLabel = request.getsModel().getLabel();
        if (FRAC.equalsIgnoreCase(modelLabel) ) {
        	modelLabel = "-";
        }else if("\\circ".equalsIgnoreCase(modelLabel) || "o".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "0";
        }else if("\\epsilon".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "E";
        }else if("\\cdots".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "\\ldots";
        }else if("\\equiv".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "\\sum";
        }else if("\\prod".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "\\pi";
        }else if("'".equalsIgnoreCase(modelLabel)) {
        	modelLabel = ")";
        }else if(",".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "COMMA";
        }else if("}".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "\\}}";
        }else if("{".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "\\{}";
        }else if("\\approx_2".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "=";
        }else if("D".equalsIgnoreCase(modelLabel)) {
        	modelLabel = "B";
        }
        for (int i = result.numberOFCandidates() - 5; i < result.numberOFCandidates(); i++) {
			if(modelLabel.equalsIgnoreCase(result.getCandidateAt(i).getLabel()) && (1.0 - result.getCandidateAt(i).getConfidence()) < cost) {
				cost = 1.0 - result.getCandidateAt(i).getConfidence();
			}
		}
		return cost;
	}

	private br.usp.ime.vision.frank.classifier.Symbol createClassifierSymbol(List<Stroke> request) {
		br.usp.ime.vision.frank.classifier.Symbol classifierSymbol = new br.usp.ime.vision.frank.classifier.Symbol();
		Point2D pAux = null;
		
        for (Stroke stroke: request) {
        	br.usp.ime.vision.frank.classifier.Stroke classifierStroke = new br.usp.ime.vision.frank.classifier.Stroke();
        	for (Point p : stroke.getPoints()) {
        		pAux = new Point2D.Double(p.getX(), p.getY());
        		classifierStroke.addCheckingBoundingBox(TimePoint.newInstanceFromPoint2D(pAux));
        	}
        	classifierSymbol.addCheckingBoundingBox(classifierStroke);
        }        
        //END OF THE SYMBOL CREATION
		return classifierSymbol;
	}

	@Override
	public ClassificationResult matchSymbol(List<Stroke> strokes) {

		br.usp.ime.vision.frank.classifier.Symbol classifierSymbol = createClassifierSymbol(strokes);
        Classifiable classifible = new Classifiable();
        double[] features = NeuralNetworkFeatures.extractMergedFeatures(classifierSymbol);
        classifible.setFeatures(features);
        NeuralNetworkClassifier classifier = NeuralNetworkClassifierSingleton.getInstance();
        classifier.classify(classifible);
        ClassificationResult result = classifier.getClassificationResult();
        result.ascendingSortByConfidence();
        
		return result;
	}

	@Override
	public SymbolClassifierResponse matchSymbol(
			SymbolClassifierRequest<Symbol> request) {
		// TODO Auto-generated method stub
		return null;
	}

}
