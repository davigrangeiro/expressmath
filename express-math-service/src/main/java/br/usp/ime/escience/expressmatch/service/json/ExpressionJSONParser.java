package br.usp.ime.escience.expressmatch.service.json;

import java.lang.reflect.Type;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.service.json.generic.JSONParseable;

@Service
public class ExpressionJSONParser extends JSONParseable<Expression> {

	@Override
	public GsonBuilder getSpecificGsonBuilder() {
		GsonBuilder builder = new GsonBuilder();

		// Register an adapter to manage the date types as long values
		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

			@Override
			public Date deserialize(JsonElement json, Type typeOfT,
					JsonDeserializationContext context)
					throws JsonParseException {
				try {
					return new Date(json.getAsJsonPrimitive().getAsLong());
				} catch (NumberFormatException e) {
					return new Date(json.getAsJsonPrimitive().getAsString());
				}
			}
		});
		
		return builder;
	}

	@Override
	public Class<Expression> getTypeClass() {
		return Expression.class;
	}

	@Override
	public Class<Expression[]> getArrayTypeClass() {
		return Expression[].class;
	}

}
