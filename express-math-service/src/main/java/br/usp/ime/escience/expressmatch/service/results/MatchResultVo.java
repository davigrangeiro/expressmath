package br.usp.ime.escience.expressmatch.service.results;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import br.usp.ime.escience.expressmatch.model.UserInfo;

public class MatchResultVo implements Serializable{

	private static final long serialVersionUID = 1L;

	private String name;
	private UserInfo userInfo;
	private Date matchDate;
	
	private List<ResultInstance> resultInstances;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Date getMatchDate() {
		return matchDate;
	}

	public void setMatchDate(Date matchDate) {
		this.matchDate = matchDate;
	}

	public List<ResultInstance> getResultInstances() {
		return resultInstances;
	}

	public void setResultInstances(List<ResultInstance> resultInstances) {
		this.resultInstances = resultInstances;
	}

}
