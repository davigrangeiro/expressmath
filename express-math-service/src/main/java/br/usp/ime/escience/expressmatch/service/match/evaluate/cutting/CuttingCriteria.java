package br.usp.ime.escience.expressmatch.service.match.evaluate.cutting;

import br.usp.ime.escience.expressmatch.service.match.evaluate.SymbolMatchHypothesis;

public interface CuttingCriteria {

	boolean isToCutHypothesis(SymbolMatchHypothesis hypothesis);
	
}
