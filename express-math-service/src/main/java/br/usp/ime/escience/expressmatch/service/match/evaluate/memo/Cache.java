package br.usp.ime.escience.expressmatch.service.match.evaluate.memo;

import java.util.HashMap;
import java.util.Map;

public class Cache<T extends Cacheable> {

	private Map<String, T> map = new HashMap<>();
	
	public boolean containsValue(T t) {
		return this.map.containsKey(t.getStringKey());
	}
	
	
	public void add(T t) {
		this.map.put(t.getStringKey(), t);
	}
	
	
	public T getValueFor(T t) {
		return this.map.get(t.getStringKey());
	}
	
	
}
