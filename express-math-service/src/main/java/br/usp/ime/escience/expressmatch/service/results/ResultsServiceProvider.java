package br.usp.ime.escience.expressmatch.service.results;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.usp.ime.escience.expressmatch.model.MatchRequest;
import br.usp.ime.escience.expressmatch.model.SimulatedExpressionMatch;
import br.usp.ime.escience.expressmatch.model.repository.MatchRequestRepository;
import br.usp.ime.escience.expressmatch.model.repository.SimulatedExpressionMatchRepository;

@Service
@Transactional
public class ResultsServiceProvider  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Autowired
	private SimulatedExpressionMatchRepository expressionMatchRepository;
	
	@Autowired
	private MatchRequestRepository matchRequestRepository;
	
	public List<MatchRequest> getMatchRequests() {
		List<MatchRequest> res = matchRequestRepository.findAll();
		return res;
	}
	
	public List<MatchResultVo> getMatchResults(){
		List<MatchResultVo> res = new ArrayList<MatchResultVo>();
		
		List<SimulatedExpressionMatch> results = expressionMatchRepository.findAll();
		Map<String, List<SimulatedExpressionMatch>>  map = new HashMap<String, List<SimulatedExpressionMatch>>();
		
		for (SimulatedExpressionMatch simulatedExpressionMatch : results) {
			 List<SimulatedExpressionMatch> list = map.remove(simulatedExpressionMatch.getStringKey());
			 
			 if(list == null) {
				 list = new ArrayList<SimulatedExpressionMatch>();
			 }
			 list.add(simulatedExpressionMatch);
			 
			 map.put(simulatedExpressionMatch.getStringKey(), list);
		}
		
		
		for (Entry<String, List<SimulatedExpressionMatch>> entry : map.entrySet()) {
			SimulatedExpressionMatch base = entry.getValue().get(0);
			MatchResultVo vo = new MatchResultVo();
			vo.setMatchDate(base.getInsertDate());
			vo.setName(base.getMatch().getMatchRequest().getRequestName());
			vo.setUserInfo(base.getMatch().getUserInfo());
			vo.setResultInstances(new ArrayList<ResultInstance>());
			
			Map<Integer, List<SimulatedExpressionMatch>>  innerMap = new HashMap<Integer, List<SimulatedExpressionMatch>>();
			
			for (SimulatedExpressionMatch simulatedExpressionMatch : entry.getValue()) {
				 List<SimulatedExpressionMatch> list = innerMap.remove(simulatedExpressionMatch.getMatch().getExpressionType().getId());
				 
				 if(list == null) {
					 list = new ArrayList<SimulatedExpressionMatch>();
				 }
				 list.add(simulatedExpressionMatch);
				 
				 innerMap.put(simulatedExpressionMatch.getMatch().getExpressionType().getId(), list);
			}
			
			
			for (Entry<Integer, List<SimulatedExpressionMatch>> innerEntry : innerMap.entrySet()) {
				SimulatedExpressionMatch innerBase = innerEntry.getValue().get(0);
				ResultInstance resultInstance = new ResultInstance();
				resultInstance.setExpressionType(innerBase.getMatch().getExpressionType());
				
				float accumulated = 0.f;
				for (SimulatedExpressionMatch simulatedExpressionMatch : innerEntry.getValue()) {
					accumulated += ((simulatedExpressionMatch.getCorrectMatchedSize() + 0.f) / (simulatedExpressionMatch.getModelSize()+0.f));
				}
				
				resultInstance.setResult(accumulated/innerEntry.getValue().size());
				vo.getResultInstances().add(resultInstance);
			}
			
			res.add(vo);
		}
		
		
		return res;
	}
	
	
}
