select 
    m.expression_type_id,
    m.user_info_id,
    t.label,
    sum(s.correct_matched_size / s.model_size) / count(*) as result,
    count(*) 
from
    expression_match m
        inner join
    simulated_expression_match s ON m.id = s.id
        inner join
    expression_type t ON m.expression_type_id = t.id
group by m.user_info_id , m.expression_type_id
order by result desc;

select sum(r.result) / count(*), r.mr from 
(select 
    sum(s.correct_matched_size / s.model_size) / count(*) as result,
	m.match_request_id as mr
from
    expression_match m
        inner join
    simulated_expression_match s ON m.id = s.id
        inner join
    expression_type t ON m.expression_type_id = t.id
group by m.user_info_id , m.expression_type_id, m.match_request_id
order by result desc) r group by r.mr;