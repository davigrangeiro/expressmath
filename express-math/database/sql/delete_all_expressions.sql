delete from expressMatch.shape_descriptor;
delete from expressMatch.point;
delete from expressMatch.stroke;
delete from expressMatch.symbol;
update expressMatch.expression set expression_type_id = null;
delete from expressMatch.expression_type;
delete from expressMatch.expression;



delete from expressMatch.stroke where id in (select s.id from expressMatch.stroke s where (select count(*) from expressMatch.point p where s.id = p.id_stroke) = 0);