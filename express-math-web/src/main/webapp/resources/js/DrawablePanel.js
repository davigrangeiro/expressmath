/**
 * class will be used to control the states of the panel.
 */

function HandwritenPanel() {

	var ZERO = 0;
	var DRAWING = 1;
	var SELECTING = 2;
	var NOT_DRAWING = ZERO;
	var LABELED_STROKE = '#157D63';
	var SELECTED_STROKE = '#DD2288';
	var DEFAULT_STROKE = '#333333';
	var CONTEXT_2D = "2d";
	var BORDER = 20;
	var PANEL_MIN_SIZE_X = 215;
	var PANEL_MIN_SIZE_Y = 130;	
	var id = 0;

	this.currentState = DRAWING;
	this.lastX = "";
	this.lastY = "";
	this.drawable = true;
	this.selectable = false;
	this.state = "";
	this.canvasName = "";
	this.context = "";
	this.canvasElement = "";
	this.strokes = [];
	this.selectedStrokes = [];
	this.historicalStrokes = [];
	this.historicalSelectedStrokes = [];
	this.modelExpression = new Expression();

	this.zoon = function(h, v) {
		this.canvasElement.width = this.canvasElement.width + (BORDER * h) > PANEL_MIN_SIZE_X ? 
				this.canvasElement.width + (BORDER * h)
				: PANEL_MIN_SIZE_X;
		this.canvasElement.height = this.canvasElement.height + (BORDER * v) > PANEL_MIN_SIZE_Y ? 
				this.canvasElement.height + (BORDER * v)
				: PANEL_MIN_SIZE_Y;
		this.drawStrokes();
	};

	this.getStrokesBoundingBoxSize = function() {
		var leftCorner = this.getLeftTopCorner();
		var rightCorner = this.getRightBottonCorner();
		var res = {
			width : rightCorner.x - leftCorner.x,
			height : rightCorner.y - leftCorner.y
		};
		return res;
	};

	this.zoon = function(event) {
		var mousePosition = this.getNewMouseCordinates(event);
		mousePosition.x -= this.canvasElement.offsetLeft;
		mousePosition.y -= this.canvasElement.offsetTop;
		this.canvasElement.width = (mousePosition.x);
		this.canvasElement.height = (mousePosition.y + 80);
		
		if(this.canvasElement.width < PANEL_MIN_SIZE_X){
			this.canvasElement.width = PANEL_MIN_SIZE_X;
		}
		if(this.canvasElement.height < PANEL_MIN_SIZE_Y){
			this.canvasElement.height = PANEL_MIN_SIZE_Y;
		}
		
		this.drawStrokes();
	};

	this.init = function(_canvasName, _drawable, _jsonString, _selectable) {
		this.lastX = ZERO;
		this.lastY = ZERO;
		this.strokes = this.fromJSONString(_jsonString);
		this.drawable = _drawable;
		this.selectable = _selectable;
		this.currentState = DRAWING;
		this.state = NOT_DRAWING;
		this.canvasName = _canvasName;
		this.canvasElement = this.getCanvasElement(this.canvasName);
		this.context = this.getNew2DContext();
		this.registraEventos();
		this.startCanvas();
	};

	this.startCanvas = function() {
		if (this.strokes.length > 0) {
			if (this.drawable) {
					// aprenas trascrevendo expressões.
					this.scaleStrokes();
					this.drawStrokes();
			} else {
				if (this.selectable) {
					// correção de expressões ja rotuladas
					this.scaleStrokes();
					this.drawStrokes();
				} else {
					// exibindo expressões
					this.drawScaledStrokes('');
				}
			}
		}
	};
	
	this.changeState = function(_state) {
		this.currentState = _state;
	}
	
	
	this.setDrawing = function() {
		this.currentState = DRAWING;
	}

	this.setSelecting = function() {
		this.currentState = SELECTING;
	}
	
	
	this.clearSelection = function() {
		var currentStroke;
		while (this.selectedStrokes.length > 0) {
			currentStroke = this.selectedStrokes.pop();
			currentStroke.color = DEFAULT_STROKE;
		}
	}

	
	this.scaleStrokes = function() {
		 var scale = this.getExpressionScale();
		 var leftCorner = this.getLeftTopCorner();
		 var bBox = this.getStrokesBoundingBoxSize();
		 var newExpressionSize = {width: (bBox.width*scale), height: (bBox.height*scale)}
		 bBox.width = (this.canvasElement.width - newExpressionSize.width)/2.0;
		 bBox.height = (this.canvasElement.height - newExpressionSize.height)/2.0;
		 leftCorner = {x: leftCorner.x *scale, y: leftCorner.y *scale}
		 bBox.width = (bBox.width - leftCorner.x);
		 bBox.height = (bBox.height - leftCorner.y);
		
		
		for ( var i = 0; i < this.strokes.length; i = i + 1) {
			for ( var j = 0; j < this.strokes[i].points.length; j =  j + 1){
				this.strokes[i].points[j].x = this.strokes[i].points[j].x * scale + bBox.width;
				this.strokes[i].points[j].y = this.strokes[i].points[j].y * scale + bBox.height;
			}
		}
	};
	
	this.scaleCanvas = function() {
		var rightCorner = this.getRightBottonCorner();
		if (rightCorner.x > this.canvasElement.width) {
			this.canvasElement.width = rightCorner.x + BORDER;
		}
		if (rightCorner.y > this.canvasElement.height) {
			this.canvasElement.height = rightCorner.y + BORDER;
		}
	};

	this.getExpressionScale = function() {
		var leftCorner = this.getLeftTopCorner();
		var rightCorner = this.getRightBottonCorner();
		
		var scales = {
			x : this.canvasElement.width
					/ (rightCorner.x - leftCorner.x + 2 * BORDER),
			y : this.canvasElement.height
					/ (rightCorner.y - leftCorner.y + 2 * BORDER)
		};

		if (scales.x < scales.y) {
			if (scales.x > 1.0) {
				scales.x = 1.0;
			}
			return scales.x;
		} else {
			if (scales.y > 1.0) {
				scales.y = 1.0;
			}
			return scales.y;
		}
		
	};

	this.getLeftTopCorner = function() {
		var leftTopCorner = {
			x : 500000,
			y : 500000
		};
		for ( var i = 0; i < this.strokes.length; i++) {
			for ( var j = 0; j < this.strokes[i].points.length; j++) {
				if (this.strokes[i].points[j].x < leftTopCorner.x) {
					leftTopCorner.x = this.strokes[i].points[j].x;
				}
				if (this.strokes[i].points[j].y < leftTopCorner.y) {
					leftTopCorner.y = this.strokes[i].points[j].y;
				}
			}
		}
		return leftTopCorner;
	};

	this.getRightBottonCorner = function() {
		var rightBottonCorner = {
			x : 0,
			y : 0
		};
		for ( var i = 0; i < this.strokes.length; i++) {
			for ( var j = 0; j < this.strokes[i].points.length; j++) {
				if (this.strokes[i].points[j].x > rightBottonCorner.x) {
					rightBottonCorner.x = this.strokes[i].points[j].x;
				}
				if (this.strokes[i].points[j].y > rightBottonCorner.y) {
					rightBottonCorner.y = this.strokes[i].points[j].y;
				}
			}
		}
		return rightBottonCorner;
	};

	this.resetCanvas = function() {
		this.strokes = [];
		this.clearCanvas();
	};

	this.asJSONString = function() {
		if (this.selectable) {
			return JSON.stringify(this.modelExpression);
		} else {
			return JSON.stringify(this.strokes);
		}
	};

	this.fromJSONString = function(jSonString) {
		if (0 != jSonString.length) {
			var loaded = JSON.parse(jSonString);
			if (!(loaded instanceof Array)) {
				this.modelExpression = loaded;
				loaded = [];
				for (var i = 0; i < this.modelExpression.symbols.length; i++) {
					for (var j = 0; j < this.modelExpression.symbols[i].strokes.length; j++) {
						loaded.push(this.modelExpression.symbols[i].strokes[j]);
					}
				}
			}
			for (var i = 0; i < loaded.length; i++) {
				if (!loaded[i].color) {
					loaded[i].color = DEFAULT_STROKE;
				}
			}
			return loaded;
		}
		return [];
	};

	this.clearCanvas = function() {
		this.canvasElement.width = this.canvasElement.width;
		this.canvasElement.height = this.canvasElement.height;
		this.context.clearRect(ZERO, ZERO, this.canvasElement.width,
				this.canvasElement.height);
		this.context = this.getNew2DContext();
	};

	this.setXYValues = function(position) {
		this.lastX = position.x;
		this.lastY = position.y;
	};

	this.getNewMouseCordinates = function(event) {
		var clientRect = this.canvasElement.getBoundingClientRect();
		var mousePosition = {
			x : event.clientX - clientRect.left,
			y : event.clientY - clientRect.top
		};
		return mousePosition;
	};

	this.stopLine = function(event) {
		this.state = NOT_DRAWING;
	};

	this.beginLine = function(event) {
		id = id + 1;
		var mousePosition = this.getNewMouseCordinates(event);
		this.defineStrokeStyle();
		
		this.clearHistoricalStrokes();

		this.currentStroke = new Stroke(id);
		this.currentStroke.points.push(mousePosition);
		
		if (this.currentState == DRAWING) {
			this.strokes.push(this.currentStroke);
		}
		this.state = this.currentState;
		
		this.setXYValues(mousePosition);

	};

	this.get2DContext = function() {
		return this.canvasElement.getContext(CONTEXT_2D);
	};
	

	this.defineStrokeStyle = function() {
		if (this.currentState == DRAWING) {
			this.context.lineWidth = 2;
			this.context.strokeStyle = DEFAULT_STROKE;
		} else {
			this.context.lineWidth = 3;
			this.context.strokeStyle = '#333399';
		}
	};

	this.getNew2DContext = function() {
		var cxt = this.get2DContext();
		cxt.lineWidth = 2;
		cxt.strokeStyle = DEFAULT_STROKE;
		cxt.lineCap = "round";
		return cxt;
	};

	this.getCanvasElement = function(canvasName) {
		return document.getElementById(canvasName);
	};

	this.drawScaledStrokes = function(color) {
		 this.clearCanvas();
		 var scale = this.getExpressionScale();
		 var leftCorner = this.getLeftTopCorner();
		 var bBox = this.getStrokesBoundingBoxSize();
		 var newExpressionSize = {width: (bBox.width*scale), height: (bBox.height*scale)}
		 bBox.width = (this.canvasElement.width - newExpressionSize.width)/2.0;
		 bBox.height = (this.canvasElement.height - newExpressionSize.height)/2.0;
		 leftCorner = {x: leftCorner.x *scale, y: leftCorner.y *scale}
		 bBox.width = (bBox.width - leftCorner.x);
		 bBox.height = (bBox.height - leftCorner.y);
		
		
		var strokesToPrint = [];
		for ( var i = 0; i < this.strokes.length; i = i + 1) {
			if (this.strokes[i].color == color) {
				strokesToPrint.push(this.strokes[i]);
			} else {
				this.drawStroke(this.strokes[i], scale, bBox.width , bBox.height);
			}
		}
		for ( var i = 0; i < strokesToPrint.length; i = i + 1) {
			this.drawStroke(strokesToPrint[i], scale, bBox.width , bBox.height, true);
		} 
	};

	this.drawStrokes = function(color) {
		this.clearCanvas();
		var strokesToPrint = [];
		for ( var i = 0; i < this.strokes.length; i = i + 1) {
			if (color && this.strokes[i].color == color) {
				strokesToPrint.push(this.strokes[i]);
			} else {
				this.drawStroke(this.strokes[i], 1.0, 0.0, 0.0);
			}
		}
		for ( var i = 0; i < strokesToPrint.length; i = i + 1) {
			this.drawStroke(strokesToPrint[i], 1.0, 0.0, 0.0, true);
		}
	};
	
	this.drawStroke = function(stroke, scale, xOff, yOff, highlight) {
		this.setXYValues(stroke.points[0]);
		this.selectStrokeLineWidth(highlight);
		this.context.strokeStyle = stroke.color;
		this.context.beginPath();
		
		for ( var i = 1; i < stroke.points.length; i = i + 1) {
			
			this.context.moveTo((this.lastX * scale) + xOff, (this.lastY * scale) + yOff);
			this.context.lineTo((stroke.points[i].x * scale) + xOff, (stroke.points[i].y * scale) + yOff);
			this.setXYValues(stroke.points[i]);
			
		}
		this.context.stroke();
	};
	
	this.selectStrokeLineWidth = function (highlight) {
		if (highlight) {
			this.context.lineWidth = 5;
		} else {
			this.context.lineWidth = 2;
		}
	}

	this.continueLine = function(event) {
		if (this.state == DRAWING || this.state == SELECTING ) {
			var mousePosition = this.getNewMouseCordinates(event);
			if (!this.isSamePoint(mousePosition)) {
				this.currentStroke.points.push(mousePosition);
				this.drawLineSegment(mousePosition);
			}
		}
	};

	this.drawLineSegment = function(mousePosition) {
		this.context.beginPath();
		this.context.moveTo(this.lastX, this.lastY);
		this.context.lineTo(mousePosition.x, mousePosition.y);
		this.context.stroke();
		this.setXYValues(mousePosition);
	};

	this.endLine = function(event) {
		this.continueLine(event);
		this.selectStrokes();
		this.state = NOT_DRAWING;
	};
	
	
	this.selectStrokes = function() {
		if (this.state == SELECTING) {
			for (var i = 0; i < this.strokes.length; i++) {
				if (this.currentStroke.isThereIntersection(this.strokes[i]) && this.strokes[i].color == DEFAULT_STROKE) {
					this.strokes[i].color = SELECTED_STROKE;
					this.selectedStrokes.push(this.strokes[i]);
				}
			}
			this.drawStrokes(SELECTED_STROKE);
		}
	}
	

	this.isSamePoint = function(mousePosition) {
		return (this.lastX == mousePosition.x && this.lastY == mousePosition.y);
	};

	this.undo = function() {
		if (this.currentState == DRAWING && this.strokes.length > 0) {
			this.historicalStrokes.push(this.strokes.pop());
			this.drawStrokes();
		}
		if (this.currentState == SELECTING && this.selectedStrokes.length > 0) {
			var stroke = this.selectedStrokes.pop();
			stroke.color = DEFAULT_STROKE;
			this.historicalSelectedStrokes.push(stroke);
			this.drawStrokes(SELECTED_STROKE);
		}
	};

	this.redo = function() {
		if (this.currentState == DRAWING && this.strokes.length > 0) {
			this.strokes.push(this.historicalStrokes.pop());
			this.drawStrokes();
		}
		if (this.currentState == SELECTING && this.historicalSelectedStrokes.length > 0) {
			var stroke = this.historicalSelectedStrokes.pop();
			stroke.color = SELECTED_STROKE;
			this.selectedStrokes.push(stroke);
			this.drawStrokes(SELECTED_STROKE);
		}
	};

	this.clearHistoricalStrokes = function() {
		this.historicalStrokes.length = ZERO;
	};
	
	this.highlightStrokeByColor = function(color) {
		this.clearCanvas();
		if (this.drawable) {
			this.drawStrokes(color);
		} else {
			this.drawScaledStrokes(color);
		}
	};
	
	this.confirmSelection = function() {
		if (this.selectedStrokes.length == 0) {
			alert("You should select some strokes before confirm selection.");
		} else {
			var href = '';
			while (href == '') {
				href = prompt("Please, provide a unique href for the selected strokes.", "Href");
				if (href != '') {
					for (var i = 0; i < this.modelExpression.symbols.length && href != ''; i++) {
						if (href == this.modelExpression.symbols[i].href) {
							alert ('Please provide a unique href.');
							href = '';
						}
					}
				}
			}
			var symbol = new Symbol();
			symbol.href = href;
			for (var i = 0; i < this.selectedStrokes.length; i++) {
				symbol.strokes.push(this.selectedStrokes[i]);
				this.selectedStrokes[i].color = LABELED_STROKE;
			}
			this.modelExpression.symbols.push(symbol);
			
			this.selectedStrokes = [];
			this.historicalSelectedStrokes = [];
			this.drawStrokes(LABELED_STROKE);
		}
	}
	
	
	this.validateResults = function() {
		var valid = true;
		for (var i = 0; i < this.strokes.length && valid; i++) {
			valid = valid && this.strokes[i].color == LABELED_STROKE;
		}
		if (!valid) {
			alert ('You should label all strokes before save');
			this.drawStrokes(DEFAULT_STROKE);
		}
		return valid;
	}
	

	this.registraEventos = function() {
		if (this.drawable) {
			
			var handwritenPanel = this;
			this.canvasElement.addEventListener('mousemove', function(e) {
				handwritenPanel.continueLine(e);
			}, false);
			this.canvasElement.addEventListener('mousedown', function(e) {
				handwritenPanel.beginLine(e);
			}, false);
			this.canvasElement.addEventListener('mouseup', function(e) {
				handwritenPanel.endLine(e);
			}, false);
			this.canvasElement.addEventListener('mouseover', function(e) {
				handwritenPanel.stopLine();
			}, false);
			this.canvasElement.addEventListener('touchmove', function(e) {
				handwritenPanel.continueLine(e);
			}, false);
			this.canvasElement.addEventListener('touchstart', function(e) {
				handwritenPanel.beginLine(e);
			}, false);
			this.canvasElement.addEventListener('touchend', function(e) {
				handwritenPanel.endLine(e);
			}, false);
			this.canvasElement.addEventListener('touchleave', function(e) {
				handwritenPanel.stopLine();
			}, false);
		}
	};
};

function Stroke(identifier) {

	var DEFAULT_STROKE_COLOR = '#333333';
	
	this.id = identifier;
	this.strokeId = 0;
	this.time = new Date();
	this.time = this.time.setHours(this.time.getHours() - this.time.getTimezoneOffset() / 60);
	this.points = [];
	this.color = DEFAULT_STROKE_COLOR;
	
	
	this.isThereIntersection = function(otherStroke) {
		var det = 0.0;
		var isThereIntersection = false;
		
		var otherStrokePoint;
		var point = this.points.length > 0 ? this.points[0] : null;
		var s1, s2;
		
		for (var i = 1; i < this.points.length && !isThereIntersection; i++) {
			otherStrokePoint = otherStroke.points.length > 0 ? otherStroke.points[0] : null;
			for (var j = 1; j < otherStroke.points.length && !isThereIntersection; j++) {
				det = ((otherStroke.points[j].x - otherStrokePoint.x) * (this.points[i].y - point.y)) -  
					  ((otherStroke.points[j].y - otherStrokePoint.y) * (this.points[i].x - point.x));
				
				if ( det != 0 ) {
					s1 = (((otherStroke.points[j].x - otherStrokePoint.x) * (otherStrokePoint.y - point.y)) -  
					  	  ((otherStroke.points[j].y - otherStrokePoint.y) * (otherStrokePoint.x - point.x))) / det;
					s2 = (((this.points[i].x - point.x) * (otherStrokePoint.y - point.y)) -  
					  	  ((this.points[i].y - point.y) * (otherStrokePoint.x - point.x))) / det;

					isThereIntersection = (s1 >= 0 && s1 <= 1) && (s2 >= 0 && s2 <= 1);
				}
				
				
				otherStrokePoint = otherStroke.points[j];
			}
			point = this.points[i];
		}
		
		return isThereIntersection;
	}
	
};


function Symbol(_label, _href) {
	
	this.strokes = [];
	this.href = _href;
	this.label = _label;
	
};


function Expression() {
	
	this.symbols = [];
	this.label;
	
};