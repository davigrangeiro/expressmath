package br.usp.ime.escience.expressmatch.controller.evaluate;

import java.io.Serializable;

public class ColorBySymbol implements Serializable, Comparable<ColorBySymbol>{

	private static final long serialVersionUID = 1L; 
	
	private String color;
	private String label;
	
	public ColorBySymbol(String color, String label) {
		super();
		this.color = color;
		this.label = label;
	}

	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public int compareTo(ColorBySymbol arg0) {
		return this.label.compareTo(arg0.getLabel());
	}

	
}
