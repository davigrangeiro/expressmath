package br.usp.ime.escience.expressmatch.controller.model;

import java.io.Serializable;

import javax.faces.application.FacesMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.usp.ime.escience.expressmatch.controller.user.UserController;
import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.ExpressionType;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.service.expressions.ExpressionServiceProvider;
import br.usp.ime.escience.expressmatch.service.json.ExpressionJSONParser;
import br.usp.ime.escience.expressmatch.service.json.StrokeJSONParser;
import br.usp.ime.escience.expressmatch.utils.FacesUtils;


@Component
@Scope("session")
public class ModelExpressionPanelController implements Serializable{


	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ModelExpressionPanelController.class);

	@Autowired
	private ExpressionJSONParser expressionParser; 
	
	@Autowired
	private UserController userController;
	
	@Autowired
	private ExpressionServiceProvider expressionServiceProvider;
	
	private String jsonString;
	
	private String jsonStringForSave;
	
	private String expressionHref;
	
	private Expression expression;
	
	private ExpressionType modelExpression;

	public ModelExpressionPanelController() {
		super();
		cleanTranscriptionData();
		this.modelExpression = new ExpressionType();
	}

	private void cleanTranscriptionData() {
		this.jsonString = "";
	}
	
	public String saveExpression() {
		try {
			parseJsonString(jsonStringForSave);
			
			if (isModelExpressionInformationValid()) {
				if (null != this.expression && null != this.expression.getSymbols()) {
					this.expressionServiceProvider.createModelExpression(this.expression, this.userController.getUser(), this.modelExpression);
					FacesUtils.addMessage("Success", "Your transcription was successfully saved", null);
				} else {
					FacesUtils.addMessage("Warning", "You should transcribe the expression before save.", FacesMessage.SEVERITY_WARN);
				}
			}
			jsonString = jsonStringForSave;
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);e.printStackTrace();
			FacesUtils.addMessage("Error", "There was an error while saving the transcription", FacesMessage.SEVERITY_ERROR);
		}
		return "";
	}

	private boolean isModelExpressionInformationValid() {
		boolean res = true;
		if (this.modelExpression.getLabel()  == null || this.modelExpression.getLabel().isEmpty()) {
			FacesUtils.addMessage("Warning", "Expression Label should not be null.", FacesMessage.SEVERITY_WARN);
			res = false;
		}
		if (this.modelExpression.getName()  == null || this.modelExpression.getName().isEmpty()) {
			FacesUtils.addMessage("Warning", "Expression name should not be null.", FacesMessage.SEVERITY_WARN);
			res = false;
		}
		if (this.modelExpression.getDescription()  == null || this.modelExpression.getDescription().isEmpty()) {
			FacesUtils.addMessage("Warning", "Expression description should not be null.", FacesMessage.SEVERITY_WARN);
			res = false;
		}
		return res;
	}

	private void parseJsonString(String jsonString)
    {
		setExpression(this.expressionParser.fromJSON(jsonString));
    }

	/**
	 * @return the jsonString
	 */
	public String getJsonString() {
		return jsonString;
	}

	/**
	 * @param jsonString the jsonString to set
	 */
	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}
	
	

	public ExpressionJSONParser getExpressionParser() {
		return expressionParser;
	}
	

	public void setExpressionParser(ExpressionJSONParser expressionParser) {
		this.expressionParser = expressionParser;
	}

	
	public Expression getExpression() {
		return expression;
	}

	
	public void setExpression(Expression expression) {
		this.expression = expression;
	}

	public String getExpressionHref() {
		return expressionHref;
	}

	public void setExpressionHref(String expressionHref) {
		this.expressionHref = expressionHref;
	}

	public ExpressionType getModelExpression() {
		return modelExpression;
	}

	public void setModelExpression(ExpressionType modelExpression) {
		this.modelExpression = modelExpression;
	}

	public String getJsonStringForSave() {
		return jsonStringForSave;
	}

	public void setJsonStringForSave(String jsonStringForSave) {
		this.jsonStringForSave = jsonStringForSave;
	}
	
}
