package br.usp.ime.escience.expressmatch.controller.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.usp.ime.escience.expressmatch.controller.evaluate.ColorBySymbol;
import br.usp.ime.escience.expressmatch.controller.user.UserController;
import br.usp.ime.escience.expressmatch.exception.ExpressMatchException;
import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.ExpressionType;
import br.usp.ime.escience.expressmatch.model.MatchRequest;
import br.usp.ime.escience.expressmatch.model.Parameter;
import br.usp.ime.escience.expressmatch.model.SimulatedExpressionMatch;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.model.User;
import br.usp.ime.escience.expressmatch.model.UserInfo;
import br.usp.ime.escience.expressmatch.model.UserParameter;
import br.usp.ime.escience.expressmatch.model.status.ExpressionStatusEnum;
import br.usp.ime.escience.expressmatch.service.expressions.ExpressionServiceProvider;
import br.usp.ime.escience.expressmatch.service.expressions.importable.InkmlImportServiceProvider;
import br.usp.ime.escience.expressmatch.service.expressions.result.ExpressionMatchResultExtractor;
import br.usp.ime.escience.expressmatch.service.json.StrokeJSONParser;
import br.usp.ime.escience.expressmatch.service.parameter.ParameterServiceProvider;
import br.usp.ime.escience.expressmatch.service.results.ResultsServiceProvider;
import br.usp.ime.escience.expressmatch.utils.ColorUtils;
import br.usp.ime.escience.expressmatch.utils.FacesUtils;


@Component
@Scope("session")
public class ConfigController implements Serializable{

	private static final int NOT_STARTED_YET = -1;

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigController.class);

	@Autowired
	private ResultsServiceProvider resultsService;
	
	@Autowired
	private UserController userController;
	
	@Autowired
	private InkmlImportServiceProvider importServiceProvider;
	
	@Autowired
	private ExpressionMatchResultExtractor extractor;
	
	@Autowired
	private StrokeJSONParser strokeParser; 
	
	@Autowired
	private ExpressionMatchResultExtractor resultExtractor;
	
	@Autowired
	private ExpressionServiceProvider expressionServiceProvider;
	
	@Autowired
	private ParameterServiceProvider parameterServiceProvider;
	
	private List<UserParameter> rootParameters;
	
	private List<UserParameter> userParameters;
	
	private List<Parameter> availableUserParameters;
	
	private String jsonString;
	
	private String jsonLoadString;
	
	private Stroke strokes[];
	
	private String expressionTypeId;
	
	private String expressionUser;
	
	private String expressionStatus;
	
	private String expressionMatchResult;
	
	private List<ExpressionType> types;
	
	private List<Expression> userExpressions;
	
	private List<ColorBySymbol> colorsBySymbol;
	
	private int eTypeIndex = NOT_STARTED_YET;

	private int eUserIndex = 0;
	
	private List<MatchRequest> matchRequests;
	
	private MatchRequest matchRequest;
	
	private UserParameter newRootParameter;
	
	private UserParameter newUserParameter;
	
	public String testMatch() {
		try {
			parseJsonString(jsonString);
			
			if (null != this.strokes && this.strokes.length > 0) {
				for (Stroke stroke : strokes) {
					stroke.setStrokeId(stroke.getId());
				} 
				List<Symbol> recognizedSymbols = this.expressionServiceProvider.testMatch(this.strokes, this.types.get(this.eTypeIndex), this.userController.getUser(), this.userExpressions.get(eUserIndex));
				SimulatedExpressionMatch result = this.resultExtractor.getResultForMatch(this.userExpressions.get(eUserIndex), recognizedSymbols);
				this.setExpressionMatchResult(String.format("%,4f", (0.f + result.getCorrectMatchedSize())/ (0.f+result.getModelSize())));
				setJsonLoadStringWithMatchedSymbols(recognizedSymbols);
				
				FacesUtils.addMessage("Success", "Your transcription was successfully matched", null);
			} else {
				FacesUtils.addMessage("Warning", "You should transcribe the expression before match.", FacesMessage.SEVERITY_WARN);
			}
			
		} catch (ExpressMatchException e){
			LOGGER.error(e.getMessage(), e);

			FacesUtils.addMessage("Error", e.getMessage(), FacesMessage.SEVERITY_ERROR);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			FacesUtils.addMessage("Error", "There was an error while saving the transcription", FacesMessage.SEVERITY_ERROR);
		}
		return "";
	}
	
	@PostConstruct
	public void init() {
		if(NOT_STARTED_YET == eTypeIndex){
			FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			this.matchRequest = new MatchRequest();
			this.matchRequests = this.resultsService.getMatchRequests();
			types = this.expressionServiceProvider.loadExpressionTypes();
			
			eTypeIndex = 0;
			loadExpressionType(eTypeIndex);
			
			loadParametersInformation();
		}
	}

	private void loadParametersInformation() {
		this.setNewRootParameter(getBlankUserParameter("root"));
		this.setNewUserParameter(getBlankUserParameter(userController.getUser().getNick()));
		this.userParameters = parameterServiceProvider.findUserParameters(userController.getUser());
		this.rootParameters = parameterServiceProvider.findRootParameters();
		
		List<Parameter> parameters = getAvailableParametersForUser();
		if(parameters.size() >= 1) {
			this.newUserParameter.setParameter(parameters.get(0));
		}
		this.availableUserParameters = parameters;
	}

	private List<Parameter> getAvailableParametersForUser() {
		List<Parameter> parameters = new ArrayList(parameterServiceProvider.findParameters());
		for (int i = 0; i < parameters.size();) {
			
			boolean found = false;
			for (int j = 0; j < userParameters.size() && !found; j++) {
				if (parameters.get(i).getId().equals(userParameters.get(j).getParameter().getId())) {
					parameters.remove(i);
					found = true;
				}
			}
			
			if(!found) {
				i++;
			}
		}
		return parameters;
	}

	private void setJsonLoadStringWithMatchedSymbols(
			List<Symbol> recognizedSymbols) {

		colorsBySymbol = new ArrayList<ColorBySymbol>();
		int strokeSum = 0;
		
		for (Symbol symbol : recognizedSymbols) {
			strokeSum += symbol.getStrokes().size();
		}
		Stroke[] matchedStrokes = new Stroke[strokeSum];

		Expression model = this.expressionServiceProvider.loadExpressionForExpressionType(this.types.get(eTypeIndex));
		Stroke[] modelStrokes = getStrokesForExpression(model);
		
		int i = 0;
		for (Symbol symbol : recognizedSymbols) {
			//getting a random color for a recognized symbol
			String color = ColorUtils.getRandomColor();
			for (Stroke stroke : symbol.getStrokes()) {
				stroke.setColor(color);
				matchedStrokes[i++] = new Stroke(stroke);
			}
			
			for (Symbol sModel : model.getSymbols()) {
				if (sModel.getHref().equals(symbol.getHref())) {
					for (Stroke stroke : sModel.getStrokes()) {
						for (Stroke coloredStroke : modelStrokes) {
							if (stroke.getId().equals(coloredStroke.getId())) {
								coloredStroke.setColor(color);
							}
						}
					}
				}
			}
			
			colorsBySymbol.add(new ColorBySymbol(color, symbol.getHref()));
		}
		Collections.sort(colorsBySymbol);
		
		setJsonLoadString(this.getStrokeParser().toJSON(modelStrokes));
		setJsonString(this.getStrokeParser().toJSON(matchedStrokes));
	}
	
	private void loadExpressionType(int index) {
		colorsBySymbol = null;
		if(types != null && index < types.size()) {
			
			ExpressionType type = this.types.get(index);
		
			Stroke[] modelStrokes = getStrokesForExpression(this.expressionServiceProvider.loadExpressionForExpressionType(type));
			
			setJsonLoadString(this.getStrokeParser().toJSON(modelStrokes));
			
			cleanTranscriptionData();
			userExpressions = this.expressionServiceProvider.loadExpressionsForExpressionType(type);
			
			this.eUserIndex = 0;
			getUserExpression(type);
		} else {
			FacesUtils.addMessage("Warning", "There is no model expressions to be transcribed", FacesMessage.SEVERITY_WARN);
		}
	}

	private void getUserExpression(ExpressionType type) {
		Stroke[] userEMStrokes;
		if(null != userExpressions && !userExpressions.isEmpty()){
			this.colorsBySymbol = new ArrayList<ColorBySymbol>();
			Expression userExpression = userExpressions.get(eUserIndex);
			userEMStrokes = getStrokesForExpression(userExpression);
			setJsonString(this.getStrokeParser().toJSON(userEMStrokes));
			this.setExpressionMatchResult("");
			this.setExpressionStatus(ExpressionStatusEnum.getEnumValueFromValue(userExpression.getExpressionStatus()).getName());
			if (null != type) {
				this.setExpressionTypeId(type.getId()+"");
			}
			this.setExpressionUser(null != userExpression.getUserInfo() ? userExpression.getUserInfo().getName() : "");
		}
	}
	
	private void cleanTranscriptionData() {
		this.jsonString = "";
	}
	
	public void selectParameterTypeForUserParameter(AjaxBehaviorEvent event) {
		for (Parameter parameter : availableUserParameters) {
			if(newUserParameter.getParameter().getId().equals(parameter.getId())) {
				newUserParameter.setParameter(parameter);
				break;
			}
		}
	}
	
	
	public void addUserParameter(){
		this.parameterServiceProvider.addUserParameter(this.newUserParameter);
		loadParametersInformation();
		FacesUtils.addMessage("Success", "The user parameter was added.", FacesMessage.SEVERITY_INFO);
	}
	
	public void saveUserParameters(){
		this.parameterServiceProvider.saveUserParameters(this.userParameters);
		loadParametersInformation();
		FacesUtils.addMessage("Success", "The user parameters was saved.", FacesMessage.SEVERITY_INFO);
	}
	
	public void addRootParameter(){
		this.parameterServiceProvider.addRootParameter(this.newRootParameter);
		loadParametersInformation();
		FacesUtils.addMessage("Success", "The root parameter was added.", FacesMessage.SEVERITY_INFO);
	}

	public void deleteUserParameter(Integer id){
		this.parameterServiceProvider.deleteUserParameter(id);
		loadParametersInformation();
		FacesUtils.addMessage("Success", "The user parameter was deleted.", FacesMessage.SEVERITY_INFO);
	}
	
	private UserParameter getBlankUserParameter(String user){
		UserParameter userParameter = new UserParameter();
		userParameter.setParameter(new Parameter());
		userParameter.setUserInfo(new UserInfo());
		userParameter.getUserInfo().setUser(new User());
		userParameter.getUserInfo().getUser().setNick(user);
		return userParameter;
	}
	
	private Stroke[] getStrokesForExpression(Expression type) {
		Stroke[] modelStrokes = null;
		
		int strokeAmount = 0;
		for (Symbol symbol : type.getSymbols()) {
			strokeAmount += symbol.getStrokes().size();
		}
		
		modelStrokes = new Stroke[strokeAmount];
		int i = 0;
		
		for (Symbol symbol : type.getSymbols()) {
			for (Stroke stroke : symbol.getStrokes()) {
				modelStrokes[i++] = new Stroke(stroke);
			}
		}
		return modelStrokes;
	}
	
	public String nextUserEM(){
		eUserIndex = (eUserIndex >= userExpressions.size()-1) ? 0: eUserIndex + 1;
		getUserExpression(null);
		return "";
	}
	
	public String previousUserEM(){
		eUserIndex = (0 >= eUserIndex) ? userExpressions.size()-1: eUserIndex - 1;
		getUserExpression(null);
		
		return "";
		
	}
	
	
	public String nextEM(){
		eTypeIndex = (eTypeIndex >= types.size()-1) ? 0: eTypeIndex + 1;
		loadExpressionType(eTypeIndex);
		
		return "";
	}
	
	public String previousEM(){
		eTypeIndex = (0 >= eTypeIndex) ? types.size()-1: eTypeIndex - 1;
		loadExpressionType(eTypeIndex);
		
		return "";
		
	}
	
	private void parseJsonString(String jsonString)
    {
		setStrokes(this.getStrokeParser().arrayFromJSON(jsonString));
    }
	
	
	public String importDataBase(){
		LOGGER.info("Starting dataset import");
		importServiceProvider.importDataSet();
		FacesUtils.addMessage("Success", "Your request to database import has been sent.", FacesMessage.SEVERITY_INFO);
		return "";
	}
	
	
	public String extractExpressionResults(){
		LOGGER.info("Starting dataset result extractor");
		if (this.matchRequest == null || this.matchRequest.getDesc().isEmpty() || this.matchRequest.getRequestName().isEmpty()) {
			FacesUtils.addMessage("Warn", "Please, inform the name and motivation for another test.", FacesMessage.SEVERITY_WARN);
		} else {
			extractor.extractExpressionDatabaseResults(userController.getUser(), this.matchRequest);
			this.matchRequest = new MatchRequest();
			FacesUtils.addMessage("Success", "Your request to evaluate the system has been sent.", FacesMessage.SEVERITY_INFO);
			this.matchRequests = this.resultsService.getMatchRequests();
		}
		return "";
	}



	public UserController getUserController() {
		return userController;
	}



	public void setUserController(UserController userController) {
		this.userController = userController;
	}



	public InkmlImportServiceProvider getImportServiceProvider() {
		return importServiceProvider;
	}



	public void setImportServiceProvider(
			InkmlImportServiceProvider importServiceProvider) {
		this.importServiceProvider = importServiceProvider;
	}



	public ExpressionMatchResultExtractor getExtractor() {
		return extractor;
	}



	public void setExtractor(ExpressionMatchResultExtractor extractor) {
		this.extractor = extractor;
	}



	public StrokeJSONParser getStrokeParser() {
		return strokeParser;
	}



	public void setStrokeParser(StrokeJSONParser strokeParser) {
		this.strokeParser = strokeParser;
	}



	public ExpressionServiceProvider getExpressionServiceProvider() {
		return expressionServiceProvider;
	}



	public void setExpressionServiceProvider(
			ExpressionServiceProvider expressionServiceProvider) {
		this.expressionServiceProvider = expressionServiceProvider;
	}



	public String getJsonString() {
		return jsonString;
	}



	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}



	public String getJsonLoadString() {
		return jsonLoadString;
	}



	public void setJsonLoadString(String jsonLoadString) {
		this.jsonLoadString = jsonLoadString;
	}



	public Stroke[] getStrokes() {
		return strokes;
	}



	public void setStrokes(Stroke[] strokes) {
		this.strokes = strokes;
	}



	public List<ExpressionType> getTypes() {
		return types;
	}



	public void setTypes(List<ExpressionType> types) {
		this.types = types;
	}



	public List<Expression> getUserExpressions() {
		return userExpressions;
	}



	public void setUserExpressions(List<Expression> userExpression) {
		this.userExpressions = userExpression;
	}



	public List<ColorBySymbol> getColorsBySymbol() {
		return colorsBySymbol;
	}



	public void setColorsBySymbol(List<ColorBySymbol> colorsBySymbol) {
		this.colorsBySymbol = colorsBySymbol;
	}

	public String getExpressionTypeId() {
		return expressionTypeId;
	}

	public void setExpressionTypeId(String expressionTypeId) {
		this.expressionTypeId = expressionTypeId;
	}

	public String getExpressionUser() {
		return expressionUser;
	}

	public void setExpressionUser(String expressionUser) {
		this.expressionUser = expressionUser;
	}

	public String getExpressionStatus() {
		return expressionStatus;
	}

	public void setExpressionStatus(String expressionStatus) {
		this.expressionStatus = expressionStatus;
	}

	public String getExpressionMatchResult() {
		return expressionMatchResult;
	}

	public void setExpressionMatchResult(String expressionMatchResult) {
		this.expressionMatchResult = expressionMatchResult;
	}

	public List<MatchRequest> getMatchRequests() {
		return matchRequests;
	}

	public void setMatchRequests(List<MatchRequest> matchRequests) {
		this.matchRequests = matchRequests;
	}

	public MatchRequest getMatchRequest() {
		return matchRequest;
	}

	public void setMatchRequest(MatchRequest matchRequest) {
		this.matchRequest = matchRequest;
	}

	public List<UserParameter> getRootParameters() {
		return rootParameters;
	}

	public void setRootParameters(List<UserParameter> rootParameters) {
		this.rootParameters = rootParameters;
	}

	public List<UserParameter> getUserParameters() {
		return userParameters;
	}

	public void setUserParameters(List<UserParameter> userParameters) {
		this.userParameters = userParameters;
	}

	public UserParameter getNewRootParameter() {
		return newRootParameter;
	}

	public void setNewRootParameter(UserParameter newRootParameter) {
		this.newRootParameter = newRootParameter;
	}

	public UserParameter getNewUserParameter() {
		return newUserParameter;
	}

	public void setNewUserParameter(UserParameter newUserParameter) {
		this.newUserParameter = newUserParameter;
	}

	public List<Parameter> getAvailableUserParameters() {
		return availableUserParameters;
	}

	public void setAvailableUserParameters(List<Parameter> availableUserParameters) {
		this.availableUserParameters = availableUserParameters;
	}

}
