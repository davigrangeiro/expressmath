package br.usp.ime.escience.expressmatch.controller.symbol.classifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.usp.ime.escience.expressmatch.controller.user.UserController;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.service.json.StrokeJSONParser;
import br.usp.ime.escience.expressmatch.service.symbol.classifier.SymbolClassifierService;
import br.usp.ime.escience.expressmatch.utils.FacesUtils;
import br.usp.ime.vision.frank.classifier.CandidateLabel;
import br.usp.ime.vision.frank.classifier.ClassificationResult;


@Component
@Scope("session")
public class SymbolClassificationController implements Serializable{

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SymbolClassificationController.class);

	@Autowired
	private StrokeJSONParser strokeParser; 
	
	@Autowired
	private UserController userController;
	
	@Autowired
	@Qualifier("neuralNetworkSymbolClassifierServiceProvider")
	private SymbolClassifierService symbolClassifierService;

	private String result; 
	
	private String jsonString;
	
	private Stroke strokes[];
	

	public SymbolClassificationController() {
		super();
		cleanTranscriptionData();
	}

	private void cleanTranscriptionData() {
		this.jsonString = "";
	}
	
	public String saveStrokes() {
		try {
			parseJsonString(jsonString);
			
			if (null != this.strokes && this.strokes.length > 0) {
				for (Stroke stroke : strokes) {
					stroke.setStrokeId(stroke.getId());
				} 
				List<Stroke> strokeList = new ArrayList<>();
				for (Stroke stroke : strokes) {
					strokeList.add(stroke);
				}
				ClassificationResult result = this.symbolClassifierService.matchSymbol(strokeList);
		        StringBuilder builder = new StringBuilder();
				for (int i = result.numberOFCandidates()-1; i >= 0; i--) {
					CandidateLabel label = result.getCandidateAt(i);
					
					builder.append(label.getLabel()).append("	>>>> ").append(label.getConfidence()).append("\n");
				}
				this.result = builder.toString();
				FacesUtils.addMessage("Success", "Your symbol was successfully recognized, see results", null);
			} else {
				FacesUtils.addMessage("Warning", "You should transcribe the symbol before send.", FacesMessage.SEVERITY_WARN);
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			FacesUtils.addMessage("Error", "There was an error while saving the transcription", FacesMessage.SEVERITY_ERROR);
		}
		return "";
	}
	


	private void parseJsonString(String jsonString)
    {
		setStrokes(this.getStrokeParser().arrayFromJSON(jsonString));
    }
	
	/**
	 * @return the strokeParser
	 */
	public StrokeJSONParser getStrokeParser() {
		return strokeParser;
	}

	/**
	 * @param strokeParser the strokeParser to set
	 */
	public void setStrokeParser(StrokeJSONParser strokeParser) {
		this.strokeParser = strokeParser;
	}

	/**
	 * @return the jsonString
	 */
	public String getJsonString() {
		return jsonString;
	}

	/**
	 * @return the strokes
	 */
	public Stroke[] getStrokes() {
		return strokes;
	}

	/**
	 * @param strokes the strokes to set
	 */
	public void setStrokes(Stroke[] strokes) {
		this.strokes = strokes;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}
	
}
