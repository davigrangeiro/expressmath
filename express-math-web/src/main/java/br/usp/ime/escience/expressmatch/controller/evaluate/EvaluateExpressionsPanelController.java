package br.usp.ime.escience.expressmatch.controller.evaluate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.usp.ime.escience.expressmatch.controller.user.UserController;
import br.usp.ime.escience.expressmatch.exception.ExpressMatchException;
import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.ExpressionType;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.model.User;
import br.usp.ime.escience.expressmatch.service.expressions.ExpressionServiceProvider;
import br.usp.ime.escience.expressmatch.service.json.StrokeJSONParser;
import br.usp.ime.escience.expressmatch.utils.ColorUtils;
import br.usp.ime.escience.expressmatch.utils.FacesUtils;


@Component
@Scope("session")
public class EvaluateExpressionsPanelController implements Serializable{

	private static final int NOT_STARTED_YET = -1;

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(EvaluateExpressionsPanelController.class);

	@Autowired
	private StrokeJSONParser strokeParser; 
	
	@Autowired
	private UserController userController;
	
	@Autowired
	private ExpressionServiceProvider expressionServiceProvider;
	
	private String jsonString;
	
	private String jsonLoadString;
	
	private Stroke strokes[];
	
	private List<ExpressionType> types;
	
	private Expression userExpression;
	
	private List<ColorBySymbol> colorsBySymbol;
	
	private int eTypeIndex = NOT_STARTED_YET;

	public EvaluateExpressionsPanelController() {
		super();
		cleanTranscriptionData();
	}

	private void cleanTranscriptionData() {
		this.jsonString = "";
	}
	
	public String saveStrokes() {
		try {
			parseJsonString(jsonString);
			
			if (null != this.strokes && this.strokes.length > 0) {
				for (Stroke stroke : strokes) {
					stroke.setStrokeId(stroke.getId());
				} 
				this.expressionServiceProvider.saveTranscription(this.strokes, this.types.get(this.eTypeIndex), this.userController.getUser(), this.userExpression);
				FacesUtils.addMessage("Success", "Your transcription was successfully saved", null);
				this.nextEM();
			} else {
				FacesUtils.addMessage("Warning", "You should transcribe the expression before save.", FacesMessage.SEVERITY_WARN);
			}
			
		} catch (ExpressMatchException e){
			LOGGER.error(e.getMessage(), e);

			FacesUtils.addMessage("Error", e.getMessage(), FacesMessage.SEVERITY_ERROR);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			FacesUtils.addMessage("Error", "There was an error while saving the transcription", FacesMessage.SEVERITY_ERROR);
		} finally {
			loadExpressionType(eTypeIndex, this.userController.getUser());
		}
		return "";
	}
	
	public String testMatch() {
		try {
			parseJsonString(jsonString);
			
			if (null != this.strokes && this.strokes.length > 0) {
				for (Stroke stroke : strokes) {
					stroke.setStrokeId(stroke.getId());
				} 
				List<Symbol> recognizedSymbols = this.expressionServiceProvider.testMatch(this.strokes, this.types.get(this.eTypeIndex), this.userController.getUser(), this.userExpression);
				setJsonLoadStringWithMatchedSymbols(recognizedSymbols);
				
				FacesUtils.addMessage("Success", "Your transcription was successfully matched", null);
			} else {
				FacesUtils.addMessage("Warning", "You should transcribe the expression before match.", FacesMessage.SEVERITY_WARN);
			}
			
		} catch (ExpressMatchException e){
			LOGGER.error(e.getMessage(), e);

			FacesUtils.addMessage("Error", e.getMessage(), FacesMessage.SEVERITY_ERROR);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			FacesUtils.addMessage("Error", "There was an error while saving the transcription", FacesMessage.SEVERITY_ERROR);
		}
		return "";
	}
	
	

	private void setJsonLoadStringWithMatchedSymbols(
			List<Symbol> recognizedSymbols) {

		colorsBySymbol = new ArrayList<ColorBySymbol>();
		int strokeSum = 0;
		
		for (Symbol symbol : recognizedSymbols) {
			strokeSum += symbol.getStrokes().size();
		}
		Stroke[] matchedStrokes = new Stroke[strokeSum];

		Expression model = this.expressionServiceProvider.loadExpressionForExpressionType(this.types.get(eTypeIndex));
		Stroke[] modelStrokes = getStrokesForExpression(model);
		
		int i = 0;
		for (Symbol symbol : recognizedSymbols) {
			//getting a random color for a recognized symbol
			String color = ColorUtils.getRandomColor();
			for (Stroke stroke : symbol.getStrokes()) {
				stroke.setColor(color);
				matchedStrokes[i++] = new Stroke(stroke);
			}
			
			for (Symbol sModel : model.getSymbols()) {
				if (sModel.getHref().equals(symbol.getHref())) {
					for (Stroke stroke : sModel.getStrokes()) {
						for (Stroke coloredStroke : modelStrokes) {
							if (stroke.getId().equals(coloredStroke.getId())) {
								coloredStroke.setColor(color);
							}
						}
					}
				}
			}
			
			colorsBySymbol.add(new ColorBySymbol(color, symbol.getHref()));
		}
		Collections.sort(colorsBySymbol);
		
		setJsonLoadString(this.getStrokeParser().toJSON(modelStrokes));
		setJsonString(this.getStrokeParser().toJSON(matchedStrokes));
	}

	private void parseJsonString(String jsonString)
    {
		setStrokes(this.getStrokeParser().arrayFromJSON(jsonString));
    }
	
	public void init() {
		if(NOT_STARTED_YET == eTypeIndex){
			FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			types = this.expressionServiceProvider.loadExpressionTypes();
			
			eTypeIndex = 0;
			loadExpressionType(eTypeIndex, this.userController.getUser());
		}
	}

	private void loadExpressionType(int index, User user) {
		colorsBySymbol = null;
		if(types != null && index < types.size()) {
			
			ExpressionType type = this.types.get(index);
		
			Stroke[] modelStrokes = getStrokesForExpression(this.expressionServiceProvider.loadExpressionForExpressionType(type));
			
			setJsonLoadString(this.getStrokeParser().toJSON(modelStrokes));
			
			cleanTranscriptionData();
			Stroke[] userEMStrokes;
			userExpression = getUserExpression(user, type);
			
			if(null != userExpression){
				userEMStrokes = getStrokesForExpression(userExpression);
				setJsonString(this.getStrokeParser().toJSON(userEMStrokes));
			}
			
		} else {
			FacesUtils.addMessage("Warning", "There is no model expressions to be transcribed", FacesMessage.SEVERITY_WARN);
		}
	}
	
	public String nextEM(){
		eTypeIndex = (eTypeIndex >= types.size()-1) ? 0: eTypeIndex + 1;
		loadExpressionType(eTypeIndex, this.userController.getUser());
		
		return "";
	}
	
	public String previousEM(){
		eTypeIndex = (0 >= eTypeIndex) ? types.size()-1: eTypeIndex - 1;
		loadExpressionType(eTypeIndex, this.userController.getUser());
		
		return "";
		
	}

	private Expression getUserExpression(User user, ExpressionType type) {
		return this.expressionServiceProvider.loadExpressionUserForExpressionType(type, user);
	}

	private Stroke[] getStrokesForExpression(Expression type) {
		Stroke[] modelStrokes = null;
		
		int strokeAmount = 0;
		for (Symbol symbol : type.getSymbols()) {
			strokeAmount += symbol.getStrokes().size();
		}
		
		modelStrokes = new Stroke[strokeAmount];
		int i = 0;
		
		for (Symbol symbol : type.getSymbols()) {
			for (Stroke stroke : symbol.getStrokes()) {
				modelStrokes[i++] = new Stroke(stroke);
			}
		}
		return modelStrokes;
	}
	
	

	/**
	 * @return the strokeParser
	 */
	public StrokeJSONParser getStrokeParser() {
		return strokeParser;
	}

	/**
	 * @param strokeParser the strokeParser to set
	 */
	public void setStrokeParser(StrokeJSONParser strokeParser) {
		this.strokeParser = strokeParser;
	}

	/**
	 * @return the jsonString
	 */
	public String getJsonString() {
		return jsonString;
	}

	/**
	 * @param jsonString the jsonString to set
	 */
	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	/**
	 * @return the jsonLoadString
	 */
	public String getJsonLoadString() {
		return jsonLoadString;
	}

	/**
	 * @param jsonLoadString the jsonLoadString to set
	 */
	public void setJsonLoadString(String jsonLoadString) {
		this.jsonLoadString = jsonLoadString;
	}

	/**
	 * @return the strokes
	 */
	public Stroke[] getStrokes() {
		return strokes;
	}

	/**
	 * @param strokes the strokes to set
	 */
	public void setStrokes(Stroke[] strokes) {
		this.strokes = strokes;
	}

	public List<ColorBySymbol> getColorsBySymbol() {
		return colorsBySymbol;
	}

	public void setColorsBySymbol(List<ColorBySymbol> colorsBySymbol) {
		this.colorsBySymbol = colorsBySymbol;
	}
	
}
